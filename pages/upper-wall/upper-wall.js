const app = getApp()
Page({
  data: {
    wall: "false"
  },
  onLoad: function(options) {
    this.setData({
      wall: options.wall
    })
  },
  submit() {
    let vm = this;
    wx.showModal({
      title: '提示',
      content: '确认上墙吗？',
      success(res) {
        if (res.confirm) {
          app.api.getWallUrl().then(res => {
            if (res.code == app.globalData.ok) {
              if (res.data) {
                vm.setData({
                  wall: "true"
                })
              }
            } else {
              wx.showToast({
                title: res.message ? res.message : '上墙请求出错了！',
                icon: 'none'
              })
            }
          }).catch(res => {
            wx.showToast({
              title: '上墙请求出错了！',
              icon: 'none'
            })
          })
        } else if (res.cancel) {}
      }
    })
  }
})