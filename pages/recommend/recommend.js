const app = getApp();
Page({
  data: {
    current: 1,
    size: 10,
    isLastPage: false,
    listData: null,
    cacheKey: 'RECOMMEND_DATA',
    refereeUsername: '',
    shareBusinessCard: false
  },
  onLoad: function(options) {
    if (options.shareBusinessCard) {
      this.setData({
        shareBusinessCard: options.shareBusinessCard,
        memberId: options.memberId
      })
    }
    //获取分享转发页面时携带的参数
    let refereeUsername = options.username
    this.setData({
      refereeUsername: refereeUsername
    })
    // 加载上一次缓存的第一页数据
    var listData = wx.getStorageSync(this.data.cacheKey);
    this.setData({
      listData: listData
    })
    this.login();
  },
  login() {
    let vm = this;
    try {
      wx.removeStorageSync(app.globalData.access_token)
      console.log('第一步：鹊桥online进入推荐页,清除token缓存')
    } catch (e) {
      console.error(e)
    }
    wx.login({
      success: function(res) {
        console.log('第二步：获取code:' + res.code)
        let params = {
          appid: app.globalData.appid,
          code: res.code,
          refereeUsername: vm.data.refereeUsername
        }
        app.api.userLogin(params).then(res => {
          console.log(res)
          if (res.code == app.globalData.ok) {
            console.log('第三步：获取openid：' + res.data.openid)
            app.api.getToken({
              client_id: app.globalData.client_id,
              client_secret: app.globalData.client_secret,
              grant_type: app.globalData.open_id_grant_type,
              openId: res.data.openid
            }).then(res => {
              console.log(res)
              app.api.refreshToken({
                client_id: app.globalData.client_id,
                client_secret: app.globalData.client_secret,
                grant_type: app.globalData.refresh_token_grant_type,
                refresh_token: res.refresh_token
              }).then(res => {
                console.log('第四步:', res);
                if (res.code == app.globalData.ok) {
                  wx.setStorageSync(app.globalData.access_token, res.data.accessToken)
                  wx.setStorageSync(app.globalData.refresh_token, res.data.refreshToken);
                  console.log('第四步：获取access_token:' + res.data.accessToken);
                  vm.currentUser();
                  vm.getData()
                  if (vm.data.shareBusinessCard) {
                    wx.navigateTo({
                      url: '../../pages/business-card/business-card?memberId=' + vm.data.memberId,
                    })
                  }
                }
              }).catch(res => {})
            }).catch(res => {})

          }
        }).catch(res => {})
      }
    })
  },
  onPullDownRefresh() {
    this.setData({
      current: 1,
      size: 10,
      isLastPage: false
    })
    this.getData()
  },
  onReachBottom() {
    if (this.data.isLastPage) {
      wx.showToast({
        title: '没有更多了',
        icon: 'none',
        duration: 1000
      })
      return
    }
    let current = this.data.current + 1;
    this.setData({
      current: current
    })
    this.getData()
  },
  currentUser() {
    app.api.currentUser({}).then(res => {
      if (res.code == app.globalData.ok) {
        app.globalData.currentUser = res.data;
      }
    })
    app.api.getUserInfo()
      .then(res => {
        if (res.code == app.globalData.ok) {
          app.globalData.currentUser.avatar = res.data.avatar;
        }
      })
  },
  getData() {
    let vm = this;
    app.api.getRecommendList({
      current: vm.data.current,
      size: vm.data.size,
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == app.globalData.ok) {
        if (res.data) {
          let listData = [];
          if (vm.data.current == 1) {
            listData = res.data.records && res.data.records.length > 0 ? res.data.records : [];
            // 缓存第一页信息，为下次进入这个页面直接展示
            wx.setStorage({
              key: vm.data.cacheKey,
              data: listData
            })
          } else {
            listData = vm.data.listData.concat(res.data.records)
          }
          let isLastPage = false;
          if (res.data.total == vm.data.current * vm.data.size || res.data.total < vm.data.current * vm.data.size) {
            isLastPage = true;
          }
          vm.setData({
            listData: listData,
            isLastPage: isLastPage
          })
        } else {
          vm.setData({
            listData: []
          })
        }

      } else {
        wx.showToast({
          title: res.message ? res.message : '推荐列表请求出错了！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.stopPullDownRefresh();
      wx.showToast({
        title: '推荐列表请求出错了！',
        icon: 'none'
      })
    })
  },
  //个人详情
  toMemberDetail(e) {
    wx.navigateTo({
      url: '../../pages/member-detail/member-detail?id=' + e.currentTarget.dataset.id,
    })
  }
})