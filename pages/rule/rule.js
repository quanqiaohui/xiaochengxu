Page({
  data: {
    title: '',
    url: ''
  },
  onLoad: function(options) {
    if (options.title) {
      wx.setNavigationBarTitle({
        title: options.title
      })
    }
    let url = '';
    if (options.title == '活动须知') {
      url = 'https://www.juyoumeng.vip/api/cms/articles/2'
    } else if (options.title == '上墙须知') {
      url = decodeURIComponent(options.wallUrl)
    } else {
      url = 'https://www.juyoumeng.vip/api/cms/articles/1'
    }
    this.setData({
      title: options.title,
      url: url
    })
  }
})