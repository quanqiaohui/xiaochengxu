const app = getApp()
Page({
  imagePath: '',
  data: {
    template: {},
    detailData: {},
    currentUser: {}
  },
  onLoad(options) {
    this.setData({
      currentUser: app.globalData.currentUser
    })
    if (options.detailData) {
      this.setData({
        detailData: JSON.parse(options.detailData)
      })
    }
  },
  onImgOK(e) {
    this.imagePath = e.detail.path;
  },

  saveImage() {
    let setPhotoAuthTime = wx.getStorageSync('setPhotoAuthTime') ? wx.getStorageSync('setPhotoAuthTime') : 1;
    //图片保存到本地
    let vm = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.imagePath,
      success: function(data) {
        wx.showToast({
          title: '保存成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail: function(err) {
        if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny" || err.errMsg === 'saveImageToPhotosAlbum:fail authorize no response') {
          if (setPhotoAuthTime == 1) {
            setPhotoAuthTime++
            wx.setStorageSync('setPhotoAuthTime', setPhotoAuthTime)
          } else {
            vm.openConfirmAuth()
          }
        } else {
          wx.showToast({
            title: err.errMsg,
            icon: 'none'
          })
        }
      }
    })
  },
  openConfirmAuth() {
    wx.showModal({
      content: '检测到您没打开保存图片权限，是否去设置打开？',
      confirmText: "确认",
      cancelText: "取消",
      success: function(res) {
        //点击“确认”时打开设置页面
        if (res.confirm) {
          wx.openSetting({
            success: (res) => {
              console.log(res)
            }
          })
        } else {
          console.log('用户点击取消')
        }
      }
    });
  },
  onReady: function() {
    let template = {
      width: '710rpx',
      height: '997rpx',
      background: '#fff',
      views: [{
          type: 'image',
          url: this.data.currentUser.avatar ? this.data.currentUser.avatar : '',
          css: {
            top: '28rpx',
            left: '19rpx',
            width: '84rpx',
            height: '84rpx',
            borderRadius: '42rpx',
          },
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: this.data.currentUser.name ? this.data.currentUser.name : '',
          css: [{
            top: '30rpx',
            left: '121rpx',
            fontWeight: 'bold',
            fontSize: '30rpx',
            color: '#333'
          }],
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: "邀你一起参加活动，快来报名吧",
          css: [{
            top: '78rpx',
            left: '121rpx',
            fontSize: '26rpx',
            color: '#333'
          }],
        },
        {
          type: 'image',
          url: this.data.detailData.image,
          css: {
            top: '133rpx',
            left: '20rpx',
            width: '670rpx',
            height: '310rpx',
            borderRadius: '10rpx',
          },
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: this.data.detailData.name,
          css: [{
            width: '516rpx',
            top: '472rpx',
            left: '20rpx',
            fontWeight: '500',
            fontSize: '34rpx',
            color: '#333',
            maxLines: 2,
            lineHeight: '50rpx'
          }],
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: "￥" + this.data.detailData.price,
          css: [{
            width: '160rpx',
            top: '472rpx',
            right: '22rpx',
            fontWeight: '600',
            fontSize: '34rpx',
            color: '#EC6669',
            maxLines: 1,
            textAlign: 'right'
          }]
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: "￥" + this.data.detailData.marketPrice,
          css: [{
            top: '521rpx',
            right: '22rpx',
            fontWeight: '600',
            fontSize: '28rpx',
            color: '#999999',
            maxLines: 1,
            textDecoration: 'line-through'
          }]
        },
        {
          type: 'image',
          url: '../../images/position.png',
          css: {
            top: '585rpx',
            left: '20rpx',
            width: '19rpx',
            height: '23rpx',
          },
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: "集合地点：" + (this.data.detailData.place ? this.data.detailData.place : ''),
          css: [{
            top: '580rpx',
            left: '49rpx',
            fontSize: '28rpx',
            color: '#999999',
            maxLines: 1,
          }]
        },
        {
          type: 'image',
          url: '../../images/time.png',
          css: {
            top: '634rpx',
            left: '20rpx',
            width: '21rpx',
            height: '21rpx',
          }
        },
        {
          id: 'my-text-id',
          type: 'text',
          text: "活动时间：" + (this.data.detailData.time ? this.data.detailData.time : ''),
          css: [{
            top: '625rpx',
            left: '49rpx',
            fontSize: '28rpx',
            color: '#999999',
            maxLines: 1,
          }]
        },
        {
          type: 'image',
          url: 'https://queqiaohui.oss-cn-hangzhou.aliyuncs.com/common/xiaochengxu.jpg',
          css: {
            bottom: '56rpx',
            left: '235rpx',
            width: '240rpx',
            height: '240rpx',
          },
        }
      ],
    }
    this.setData({
      template: template,
    });
  },
});