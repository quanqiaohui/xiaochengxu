const app = getApp()
Page({
  data: {
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    indicatorDots: false,
    vertical: false,
    autoplay: false,
    currentIdx: 1,
    detailData: {},
    showModal: false,
    memberId: '',
    wechat: ''
  },
  changeSwiper(e) {
    this.setData({
      currentIdx: e.detail.current + 1
    })
  },
  onLoad(options) {
    this.getData(options.id)
    this.setData({
      memberId: options.id
    })
  },
  getData(id) {
    var vm = this;
    app.api.getMemberDetail(id)
      .then(res => {
        if (res.code == 200) {
          if (!res.data.singleIdeal) {
            res.data.singleIdeal = {}
          }
          vm.setData({
            detailData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '个人详情请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '个人详情请求出错了！',
          icon: 'none'
        })
      })
  },
  // 打开弹窗
  openModal: function() {
    var vm = this;
    app.api.getWechat(this.data.memberId)
      .then(res => {
        if (res.code == 200) {
          if (res.data) {
            vm.setData({
              showModal: true,
              wechat: res.data
            })
          } else {
            wx.showToast({
              title: '对方还没有填写联系方式！',
              icon: 'none'
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '联系方式获取失败！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '联系方式获取失败！',
          icon: 'none'
        })
      })
  },
  // 禁止屏幕滚动
  preventTouchMove: function() {},
  // 关闭弹窗
  closeModal: function() {
    this.setData({
      showModal: false
    })
  },
  copyWechat() {
    let vm = this;
    if (vm.data.wechat) {
      wx.setClipboardData({
        data: vm.data.wechat,
        success(res) {
          wx.getClipboardData({
            success(res) {}
          })
        }
      })
    } else {
      wx.showToast({
        title: '会员暂未填写微信号',
        icon: 'none'
      })
    }
  },
  //关注/取消关注
  follow() {
    let vm = this;
    if (this.data.detailData.follow) {
      app.api.followCancel(this.data.detailData.id)
        .then(res => {
          if (res.code == 200) {
            let follow = 'detailData.follow'
            vm.setData({
              [follow]: false
            })
          } else {
            wx.showToast({
              title: res.message ? res.message : '取消关注请求出错了！',
              icon: 'none'
            })
          }
        }).catch(res => {
          wx.showToast({
            title: '取消关注请求出错了！',
            icon: 'none'
          })
        })
    } else {
      app.api.follow(this.data.detailData.id)
        .then(res => {
          if (res.code == 200) {
            let follow = 'detailData.follow'
            vm.setData({
              [follow]: true
            })
          } else {
            wx.showToast({
              title: res.message ? res.message : '关注请求出错了！',
              icon: 'none'
            })
          }
        }).catch(res => {
          wx.showToast({
            title: '关注请求出错了！',
            icon: 'none'
          })
        })
    }
  },
  //举报
  report() {
    wx.navigateTo({
      url: '../../pages/report/report?reportUserInfo=' + JSON.stringify(this.data.detailData),
    })
  }
})