const app = getApp();
Page({
  data: {
    listData: [],
    income: ''
  },
  onLoad: function (options) {
    this.getData()
  },
  //获取数据列表
  getData() {
    var vm = this;
    app.api.getDictItem({
      typeCode: 'INCOME '
    })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            listData: res.data
          })
          if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').income) {
            vm.setData({
              income: wx.getStorageSync('writeUserInfo').income
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  changeSelect(e) {
    this.setData({
      income: e.currentTarget.dataset.income
    })
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.income = this.data.income;
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-marital/member-marital',
    })
  }
})