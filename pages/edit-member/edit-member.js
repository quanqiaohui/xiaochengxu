const app = getApp()
const util = require('../../utils/util.js')
Page({
  data: {
    userInfo: {},
    educationData: [],
    educationIdx: 0,
    incomeData: [],
    incomeIdx: 0,
    workIdx: 0,
    workData: [],
    maritalData: [{
      name: '单身',
      type: 'SINGLE'
    }, {
      name: '离异',
      type: 'DIVORCE'
    }, {
      name: '丧偶',
      type: 'WIDOWED'
    }],
    codes: [],
    city: '',
    citylist: [],
    selectAreaType: '', //选择地区类型 1、选择家乡 2、选择居住地
  },
  onSelect(e) {
    if (this.data.selectAreaType == 1) {
      let domicileAreaId = 'userInfo.domicileAreaId';
      let domicileAreaName = 'userInfo.domicileAreaName'
      this.setData({
        [domicileAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [domicileAreaName]: e.detail.value.join('')
      })
    } else if (this.data.selectAreaType == 2) {
      let dwellingAreaId = 'userInfo.dwellingAreaId';
      let dwellingAreaName = 'userInfo.dwellingAreaName'
      this.setData({
        [dwellingAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [dwellingAreaName]: e.detail.value.join('')
      })
    }
  },
  onLoad: function(options) {
    this.getUserInfo()
    this.getAreas()
  },
  //选择家乡
  selectDom() {
    let codes = []
    for (let i = 0; i < this.data.userInfo.domicileAreas.length; i++) {
      codes.push(this.data.userInfo.domicileAreas[i].id)
    }
    this.setData({
      selectAreaType: 1,
      codes: codes
    })
  },
  //选择居住地
  selectDwe() {
    let codes = []
    for (let i = 0; i < this.data.userInfo.dwellingAreas.length; i++) {
      codes.push(this.data.userInfo.dwellingAreas[i].id)
    }
    this.setData({
      selectAreaType: 2,
      codes: codes
    })
  },
  getAreas() {
    let params = {},
      vm = this;
    app.api.getAreas(params)
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            citylist: res.data.options
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '地址请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '地址请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取个人信息
  getUserInfo() {
    var vm = this;
    app.api.getUserInfo()
      .then(res => {
        if (res.code == 200) {
          res.data.birth = util.formatTime(res.data.birth)
          vm.setData({
            userInfo: res.data
          })
          vm.getEducationData()
          vm.getIncomeData()
          vm.getworkData()
        } else {
          wx.showToast({
            title: res.message ? res.message : '个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取学历列表
  getEducationData() {
    var vm = this;
    app.api.getEducation()
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            educationData: res.data
          })
          let educationIdx = vm.data.educationIdx
          for (let i = 0; i < vm.data.educationData.length; i++) {
            if (vm.data.educationData[i].desc == vm.data.userInfo.educationDesc) {
              educationIdx = i
            }
          }
          vm.setData({
            educationIdx: educationIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '学历列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '学历列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取收入列表
  getIncomeData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'INCOME '
      })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            incomeData: res.data
          })
          let incomeIdx = vm.data.incomeIdx
          for (let i = 0; i < vm.data.incomeData.length; i++) {
            if (vm.data.incomeData[i].name == vm.data.userInfo.income) {
              incomeIdx = i
            }
          }
          vm.setData({
            incomeIdx: incomeIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取职业列表
  getworkData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'OCCUPATION'
      })
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            id: '',
            name: '不限'
          })
          vm.setData({
            workData: res.data
          })
          let workIdx = vm.data.workIdx
          for (let i = 0; i < vm.data.workData.length; i++) {
            if (vm.data.workData[i].name == vm.data.userInfo.work) {
              workIdx = i
            }
          }
          vm.setData({
            workIdx: workIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  selectMarital(e) {
    let maritalState = 'userInfo.maritalState'
    this.setData({
      [maritalState]: e.currentTarget.dataset.maritalstate
    })
  },
  //设置微信号
  setWechat(e) {
    let wechat = 'userInfo.wechat'
    this.setData({
      [wechat]: e.detail.value
    })
  },
  //设置昵称
  setNickname(e) {
    let nickname = 'userInfo.nickname'
    this.setData({
      [nickname]: e.detail.value
    })
  },
  //选择学历
  bindEducationChange: function(e) {
    let educationDesc = 'userInfo.educationDesc';
    let education = 'userInfo.education';
    this.setData({
      [educationDesc]: this.data.educationData[e.detail.value].desc,
      [education]: this.data.educationData[e.detail.value].education,
    })
  },
  //选择收入
  bindIncomeChange(e) {
    let income = 'userInfo.income'
    this.setData({
      [income]: this.data.incomeData[e.detail.value].name
    })
  },
  //选择职业
  bindWorkChange(e) {
    let work = 'userInfo.work'
    this.setData({
      [work]: this.data.workData[e.detail.value].name
    })
  },
  submit() {
    let userInfo = this.data.userInfo;
    if (!userInfo.wechat) {
      wx.showToast({
        title: '请填写微信号',
        icon: 'none'
      })
      return
    }
    let params = {
      "wechat": userInfo.wechat,
      "nickname": userInfo.nickname,
      "education": userInfo.education,
      "income": userInfo.income,
      "work": userInfo.work,
      "maritalState": userInfo.maritalState,
      "domicileAreaId": userInfo.domicileAreaId,
      "dwellingAreaId": userInfo.dwellingAreaId
    }
    app.api.updateUserInfo(params)
      .then(res => {
        if (res.code == 200) {
          app.globalData.writeUserInfo = {}
          wx.navigateBack({
            delta: 1
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '更新个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '更新个人信息请求出错了！',
          icon: 'none'
        })
      })
  }
})