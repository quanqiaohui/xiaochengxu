Page({
  data: {
    height150: ['150', '151', '152', '153', '154', '155', '156', '157', '158', '159', ''],
    height160: ['160', '161', '162', '163', '164', '165', '166', '167', '168', '169', ''],
    height170: ['170', '171', '172', '173', '174', '175', '176', '177', '178', '179', ''],
    height180: ['180', '181', '182', '183', '184', '185', '186', '187', '188', '189', ''],
    height190: ['190', '191', '192', '193', '194', '195', '196', '197', '198', '199', ''],
    height: 'empty'
  },
  onLoad: function(options) {
    if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').height) {
      this.setData({
        height: wx.getStorageSync('writeUserInfo').height
      })
    }
  },
  selectHeight(e) {
    if (e.currentTarget.dataset.height != 'empty') {
      this.setData({
        height: e.currentTarget.dataset.height
      })
      let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
      writeUserInfo.height = this.data.height;
      wx.setStorageSync('writeUserInfo', writeUserInfo);
      wx.navigateTo({
        url: '../../pages/member-education/member-education',
      })
    } else {
      wx.showToast({
        title: '请选择身高',
        icon: 'none'
      })
    }
  }
})