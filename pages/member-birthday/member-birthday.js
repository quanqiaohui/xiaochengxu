const date = new Date()
const years = []
const months = []
const days = []
for (let i = 1950; i <= date.getFullYear(); i++) {
  years.push(i)
}
for (let i = 0; i <= 11; i++) {
  var k = i;
  if (0 <= i && i < 9) {
    k = "0" + (i + 1);
  } else {
    k = (i + 1);
  }
  months.push(k)
}

for (let i = 1; i <= 31; i++) {
  var k = i;
  if (0 <= i && i < 10) {
    k = "0" + i
  }
  days.push(k)
}
var thisMon = date.getMonth();
var thisDay = date.getDate();
if (0 <= thisMon && thisMon < 9) {
  thisMon = "0" + (thisMon + 1);
} else {
  thisMon = (thisMon + 1);
}
if (0 <= thisDay && thisDay < 10) {
  thisDay = "0" + thisDay;
}

function mGetDate(year, month) {
  var d = new Date(year, month, 0);
  return d.getDate();
}
Page({
  data: {
    //---时间控件参数
    flag: false,
    year: date.getFullYear(),
    month: thisMon,
    day: thisDay,
    //数组中保存的可选日期
    years: years,
    months: months,
    days: days,
    value: [date.getFullYear(), thisMon - 1, thisDay - 1],
  },
  getTime: function(e) {
    var times = this.data.year + "-" + this.data.month + "-" + this.data.day
    this.setData({
      flag: true,
      upTime: times,
    });
  },
  bindChange: function(e) {
    const val = e.detail.value
    this.setData({
      year: this.data.years[val[0]],
      month: this.data.months[val[1]],
      day: this.data.days[val[2]],
    })
    var totalDay = mGetDate(this.data.year, this.data.month);
    console.log(totalDay)
    var changeDate = [];
    for (let i = 1; i <= totalDay; i++) {
      var k = i;
      if (0 <= i && i < 10) {
        k = "0" + i
      }
      changeDate.push(k)
    }
    this.setData({
      days: changeDate
    })
  },
  onLoad: function(options) {
    if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').birth) {
      let birthIdx = wx.getStorageSync('writeUserInfo').birthIdx;
      let birth = wx.getStorageSync('writeUserInfo').birth.split('-')
      this.setData({
        value: [birthIdx[0], birthIdx[1], birthIdx[2]],
        year: birth[0],
        month: birth[1],
        day: birth[2]
      })
    }
  },
  next() {
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.birth = `${this.data.year}-${this.data.month}-${this.data.day}`;
    writeUserInfo.birthIdx = [this.data.year - 1950, this.data.month - 1, this.data.day - 1];
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-height/member-height',
    })
  }
})