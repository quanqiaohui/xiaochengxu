const app = getApp()
const util = require('../../utils/util.js')
Page({
  data: {
    current: 1,
    size: 10,
    isLastPage: false,
    listData: null,
    cityList: [],
    currentCity: {}
  },
  onPullDownRefresh() {
    app.getIsHaveUserinfo('enroll');
    this.setData({
      current: 1,
      size: 10,
      areaId: '',
      isLastPage: false
    })
    this.getData()
  },
  onReachBottom() {
    if (this.data.isLastPage) {
      wx.showToast({
        title: '没有更多了',
        icon: 'none',
        duration: 1000
      })
      return
    }
    let current = this.data.current + 1;
    this.setData({
      current: current
    })
    this.getData()
  },
  onLoad: function(options) {
    app.getIsHaveUserinfo('enroll');
    this.getData()
    this.getActivityCity()
  },
  onShow() {},
  getData() {
    let vm = this;
    app.api.getActivityList('', {
      current: vm.data.current,
      size: vm.data.size,
      areaId: vm.data.currentCity && vm.data.currentCity.areaId ? vm.data.currentCity.areaId : ''
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == app.globalData.ok) {
        if (res.data) {
          if (res.data.records && res.data.records.length > 0) {
            for (var i = 0; i < res.data.records.length; i++) {
              if (res.data.records[i].price) {
                res.data.records[i].price = Number(res.data.records[i].price).toFixed(2)
              }
              if (res.data.records[i].marketPrice) {
                res.data.records[i].marketPrice = Number(res.data.records[i].marketPrice).toFixed(2)
              }
            }
          }
          let listData = [];
          if (vm.data.current == 1) {
            listData = res.data.records && res.data.records.length > 0 ? res.data.records : [];
          } else {
            listData = vm.data.listData.concat(res.data.records)
          }
          let isLastPage = false;
          if (res.data.total == vm.data.current * vm.data.size || res.data.total < vm.data.current * vm.data.size) {
            isLastPage = true;
          }
          vm.setData({
            listData: listData,
            isLastPage: isLastPage
          })
        } else {
          vm.setData({
            listData: []
          })
        }
      } else {
        wx.showToast({
          title: res.message ? res.message : '活动列表请求出错了！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.stopPullDownRefresh();
      wx.showToast({
        title: '活动列表请求出错了！',
        icon: 'none'
      })
    })
  },
  toDetail(e) {
    let sn = e.currentTarget.dataset.sn
    wx.navigateTo({
      url: '../../pages/activity-detail/activity-detail?sn=' + sn,
    })
  },
  //获取活动城市
  getActivityCity() {
    let vm = this;
    app.api.getActivityCity({}).then(res => {
      if (res.code == 200) {
        if (res.data) {
          vm.setData({
            cityList: res.data
          })
        }
      } else {
        wx.showToast({
          title: res.message ? res.message : '活动城市请求出错了！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.stopPullDownRefresh();
      wx.showToast({
        title: '活动城市请求出错了！',
        icon: 'none'
      })
    })
  },
  //选择城市
  bindCityChange: function(e) {
    if (this.data.cityList[e.detail.value] != this.data.currentCity) {
      this.setData({
        currentCity: this.data.cityList[e.detail.value]
      })
      this.setData({
        current: 1,
        size: 10,
        isLastPage: false
      })
      this.getData()
    }
  },
})