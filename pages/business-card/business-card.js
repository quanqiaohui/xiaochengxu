const app = getApp()
Page({
  data: {
    memberData: {},
  },
  onLoad: function(options) {
    this.getData(options.memberId)
  },
  getData(id) {
    var vm = this;
    app.api.getCardrDetail(id)
      .then(res => {
        if (res.code == 200) {
          if (!res.data.singleIdeal) {
            res.data.singleIdeal = {}
          }
          vm.setData({
            memberData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '名片请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '名片请求出错了！',
          icon: 'none'
        })
      })
  },
  // 分享
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: this.data.memberData.name,
      path: '/pages/recommend/recommend?shareBusinessCard=true&memberId=' + this.data.memberData.id + '&username =' + app.globalData.currentUser.username,
      imageUrl: ''
    }
  },
  shareCancel() {
    wx.navigateBack({
      delta: -1
    })
  }
})