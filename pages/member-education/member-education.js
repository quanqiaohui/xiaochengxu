const app = getApp();
Page({
  data: {
    listData: [],
    education: ''
  },
  onLoad: function (options) {
    this.getData()
  },
  //获取数据列表
  getData() {
    var vm = this;
    app.api.getEducation()
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            listData: res.data
          })
          if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').education) {
            vm.setData({
              education: wx.getStorageSync('writeUserInfo').education
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '学历列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '学历列表请求出错了！',
          icon: 'none'
        })
      })
  },
  changeSelect(e) {
    this.setData({
      education: e.currentTarget.dataset.education
    })
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.education = this.data.education;
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-income/member-income',
    })
  }
})