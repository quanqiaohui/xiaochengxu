// pages/enroll-status/enroll-status.js
const app = getApp()
Page({
  data: {
    status: '',
    info: {},
    paySigns: {},
    isAgreeRules: false,
    surplusTime: '00.00',
    minute: '00',
    second: '00',
    timeComputed: null,
    selectGoodsSn: '', //选中商品的sn
  },
  onLoad: function(options) {
    if (options.order) {
      options.order.amountPayable = options.order.amountPayable ? Number(options.order.amountPayable).toFixed(2) : '0.00'
    }
    this.setData({
      status: options.status,
      info: options.info ? JSON.parse(options.info) : {},
      selectGoodsSn: options.selectGoodsSn
    })
  },
  onReady: function() {},
  onShow: function() {
    if (this.data.status && this.data.info) {
      this.getTime()
    }
  },
  //页面卸载时关闭计时器
  onUnload() {
    clearTimeout(this.data.timeComputed);
  },
  getTime() {
    if (this.data.status == 2 && this.data.info.order.expired == false) {
      var vm = this;
      this.getRTime(vm.data.info.order.expire)
      let timeComputed = setInterval(function() {
        if (vm.data.minute == '00' && vm.data.second == '00') {
          clearTimeout(vm.data.timeComputed);
          return
        }
        vm.getRTime(vm.data.info.order.expire)
      }, 1000)
      this.setData({
        timeComputed: timeComputed
      })
    }
  },
  //支付
  toPay() {
    if (!this.data.isAgreeRules) {
      wx.showToast({
        title: '请先同意鹊桥online支付协议',
        duration: 1200,
        icon: 'none'
      })
      return
    }
    var vm = this;
    app.api.getPayParams({
      sn: vm.data.info.order.sn
    }).then(res => {
      if (res.code == 200) {
        vm.setData({
          paySigns: res.data
        })
        vm.wechatPay()
      } else {
        wx.showToast({
          title: res.message ? res.message : '支付失败！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.showToast({
        title: '支付失败！',
        icon: 'none'
      })
    })
  },
  // 微信支付
  wechatPay() {
    var vm = this;
    wx.requestPayment({
      'timeStamp': vm.data.paySigns.timeStamp + '',
      'nonceStr': vm.data.paySigns.nonceStr,
      'package': vm.data.paySigns.packageValue,
      'signType': vm.data.paySigns.signType,
      'paySign': vm.data.paySigns.paySign,
      'success': function(res) {
        wx.showToast({
          title: '支付成功',
          duration: 1200
        })
        setTimeout(function() {
          vm.setData({
            status: 3
          })
        }, 2000)
      },
      'fail': function(res) {
        wx.showToast({
          title: '微信支付失败',
          icon: 'none',
          duration: 1200
        })
      }
    })
  },

  //同意并报名(创建订单)
  agreeEnroll() {
    var vm = this;
    // 订阅通知
    wx.requestSubscribeMessage({
      tmplIds: ['ilo2dfVkbqLxn7twgryYOED_rV5ZBCm2j8mABJNU4GE'],
      success(res) {
        console.log(res)
      },
      fail(res) {
        console.log(res)
      }
    });

    app.api.createOrder({
      activitySn: vm.data.selectGoodsSn ? vm.data.selectGoodsSn : vm.data.info.sn,
    }).then(res => {
      if (res.code == 200) {
        wx.showToast({
          title: '报名成功',
          duration: 1200
        })
        setTimeout(function() {
          let order = "info.order"
          vm.setData({
            status: 2,
            [order]: res.data
          })
          vm.getRTime(vm.data.info.order.expire)
          let timeComputed = setInterval(function() {
            if (vm.data.minute == '00' && vm.data.second == '00') {
              clearTimeout(vm.data.timeComputed);
              return
            }
            vm.getRTime(vm.data.info.order.expire)
          }, 1000)
          vm.setData({
            timeComputed: timeComputed
          })
        }, 2000)
      } else {
        wx.showToast({
          title: res.message ? res.message : '报名失败！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.showToast({
        title: '报名失败！',
        icon: 'none'
      })
    })
  },
  //同意支付协议
  agreeRules() {
    let isAgreeRules = this.data.isAgreeRules;
    if (isAgreeRules) {
      this.setData({
        isAgreeRules: false
      })
    } else {
      this.setData({
        isAgreeRules: true
      })
    }
  },
  //订单剩余有效时间
  getRTime(EndTime) {
    // 结束时间
    var EndTime = EndTime
    //当前时间
    var NowTime = new Date();
    //后台给我的是10位 精确到秒的  所有下面我就除以了1000，不要小数点后面的
    var t = Number(EndTime) / 1000 - (NowTime.getTime() / 1000).toFixed(0);
    //如果后台给的是毫秒 上面不用除以1000  下面的计算时间也都要除以1000 这里我去掉1000了
    //天、时、分、秒
    var d = Math.floor(t / 60 / 60 / 24);
    var h = Math.floor(t / 60 / 60 % 24);
    var m = Math.floor(t / 60 % 60);
    var s = Math.floor(t % 60);
    if (parseInt(d) < 10) {
      d = "0" + d;
    }
    if (parseInt(h) < 10) {
      h = "0" + h;
    }
    if (parseInt(m) < 10) {
      m = "0" + m;
    }
    if (parseInt(s) < 10) {
      s = "0" + s;
    }
    this.setData({
      surplusTime: (h ? (h + ':') : '') + m + ':' + s,
      minute: m,
      second: s
    })
  },
  //返回首页
  backHome() {
    wx.navigateBack({
      delta: 2
    })
  },
  // 打开鹊桥online支付协议
  openDisRules() {
    var vm = this;
    wx.navigateTo({
      url: '../../pages/rule/rule?title=支付协议',
    })
  }
})