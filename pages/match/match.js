const app = getApp();
Page({
  data: {
    nav: ['我的报名', '所有配对', '关注我的', '我关注的'],
    currentNavIdx: 0,
    inputFocus: false,
    hiddenSelect: true,
    keyword: '', //搜索关键字
    orderPageNum: 1,
    orderIsLastPage: false,
    myOrder: null,
    pageNum: 1,
    isLastPage: false,
    listData: null,
    //筛选规则
    rule: {
      startEducation: '',
      endEducation: '',
      startEducationDesc: '',
      endEducationDesc: '',
      income: '',
      maritalState: '',
      starSign: '',
      domicileAreaId: '',
      dwellingAreaId: '',
    },
    educationIndex: [0, 0],
    educationData: [],
    incomeData: [],
    starSignData: [],
    maritalData: [{
      name: '单身',
      type: 'SINGLE'
    }, {
      name: '离异',
      type: 'DIVORCE'
    }, {
      name: '丧偶',
      type: 'WIDOWED'
    }],
    ageSectionArr: [],
    ageIdx: [],
    heightSectionArr: [],
    heightIdx: [],
    username: '',


    codes: [],
    city: '',
    citylist: [],
    selectAreaType: '', //选择地区类型 1、选择家乡 2、选择居住地
  },
  onLoad: function(options) {
    let ageSection = [],
      ageSectionArr = [];
    for (let i = 16; i < 70; i++) {
      ageSection.push(i)
    }
    ageSectionArr.push(ageSection, ageSection)
    this.setData({
      ageSectionArr: ageSectionArr
    })

    let heightSection = [],
      heightSectionArr = [];
    for (let i = 140; i < 200; i++) {
      heightSection.push(i)
    }
    heightSectionArr.push(heightSection, heightSection)
    this.setData({
      heightSectionArr: heightSectionArr
    })
    this.getEducationData()
    this.getIncomeData()
    this.getStarSignData()

    app.getIsHaveUserinfo('match');
    this.getMyEnroll();
    this.getAreas();
  },
  onShow() {
    this.setData({
      username: app.globalData.currentUser.username
    });
    if (app.globalData.matchIdx) {
      this.setData({
        currentNavIdx: app.globalData.matchIdx
      })
      this.setData({
        pageNum: 1,
        isLastPage: false
      })
      this.getData()
      app.globalData.matchIdx = 0
    }
  },
  onPullDownRefresh() {
    if (this.data.currentNavIdx == 0) {
      this.setData({
        orderPageNum: 1,
        orderIsLastPage: false
      })
      this.getMyEnroll('refresh')
    } else {
      this.setData({
        pageNum: 1,
        isLastPage: false
      })
      this.getData('refresh')
    }
  },
  onReachBottom() {
    if (this.data.currentNavIdx == 0) {
      if (this.data.orderIsLastPage) {
        wx.showToast({
          title: '没有更多了',
          icon: 'none',
          duration: 1000
        })
        return
      }
      let orderPageNum = this.data.orderPageNum + 1;
      this.setData({
        orderPageNum: orderPageNum
      })
      this.getMyEnroll()
    } else {
      if (this.data.isLastPage) {
        wx.showToast({
          title: '没有更多了',
          icon: 'none',
          duration: 1000
        })
        return
      }
      let pageNum = this.data.pageNum + 1;
      this.setData({
        pageNum: pageNum
      })
      this.getData()
    }
  },
  getMyEnroll() {
    let vm = this;
    app.api.getActivityList('/order', {
      current: vm.data.orderPageNum,
      size: 10
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == 200) {
        if (res.data) {
          if (res.data.records && res.data.records.length > 0) {
            for (var i = 0; i < res.data.records.length; i++) {
              if (res.data.records[i].price) {
                res.data.records[i].price = Number(res.data.records[i].price).toFixed(2)
              }
              if (res.data.records[i].marketPrice) {
                res.data.records[i].marketPrice = Number(res.data.records[i].marketPrice).toFixed(2)
              }
            }
          }
          let myOrder = [];
          if (vm.data.orderPageNum == 1) {
            myOrder = res.data.records && res.data.records.length > 0 ? res.data.records : [];
          } else {
            myOrder = vm.data.myOrder.concat(res.data.records)
          }
          let orderIsLastPage = false;
          if (res.data.total == vm.data.orderPageNum * 10 || res.data.total < vm.data.orderPageNum * 10) {
            orderIsLastPage = true;
          }
          vm.setData({
            myOrder: myOrder,
            orderIsLastPage: orderIsLastPage
          })
        } else {
          vm.setData({
            myOrder: [],
          })
        }
      } else {
        if (res.message) {
          wx.showToast({
            title: res.message,
            icon: 'none'
          })
        }
      }
    }).catch(res => {
      wx.stopPullDownRefresh();
      wx.showToast({
        title: '活动列表请求出错了！',
        icon: 'none'
      })
    })
  },
  getAreas() {
    let params = {
        unlimited: true
      },
      vm = this;
    app.api.getAreas(params)
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            citylist: res.data.options
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '地址请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '地址请求出错了！',
          icon: 'none'
        })
      })
  },
  getData(refresh) {
    var vm = this;
    let url = "";
    if (this.data.currentNavIdx == 1) {
      url = "mutualFollow"
    } else if (this.data.currentNavIdx == 2) {
      url = "followMe"
    } else if (this.data.currentNavIdx == 3) {
      url = "myFollow"
    }
    let rule = this.data.rule;
    let params = {
      current: this.data.pageNum,
      size: 10,
      name: this.data.keyword,
      minAge: this.data.ageSectionArr[0][this.data.ageIdx[0]] ? this.data.ageSectionArr[0][this.data.ageIdx[0]] : '',
      maxAge: this.data.ageSectionArr[1][this.data.ageIdx[1]] ? this.data.ageSectionArr[1][this.data.ageIdx[1]] : '',
      minHeight: this.data.heightSectionArr[0][this.data.heightIdx[0]] ? this.data.heightSectionArr[0][this.data.heightIdx[0]] : '',
      maxHeight: this.data.heightSectionArr[1][this.data.heightIdx[1]] ? this.data.heightSectionArr[1][this.data.heightIdx[1]] : '',
      startEducation: rule.startEducation && rule.startEducation != '不限' ? rule.startEducation : '',
      endEducation: rule.endEducation && rule.endEducation != '不限' ? rule.endEducation : '',
      income: rule.income,
      maritalState: rule.maritalState,
      starSign: rule.starSign,
      domicileAreaId: rule.domicileAreaId ? rule.domicileAreaId : '',
      dwellingAreaId: rule.dwellingAreaId ? rule.dwellingAreaId : '',
    }
    app.api.getMatchData(url, params)
      .then(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        if (res.code == 200) {
          if (res.data) {
            let listData = [];
            if (vm.data.pageNum == 1) {
              listData = res.data.records && res.data.records.length > 0 ? res.data.records : [];
            } else {
              listData = vm.data.listData.concat(res.data.records)
            }
            let isLastPage = false;
            if (res.data.total == vm.data.pageNum * 10 || res.data.total < vm.data.pageNum * 10) {
              isLastPage = true;
            }
            vm.setData({
              listData: listData,
              isLastPage: isLastPage
            })
          } else {
            vm.setData({
              listData: []
            })
          }
        } else {
          if (res.message) {
            wx.showToast({
              title: res.message,
              icon: 'none'
            })
          }
        }
      }).catch(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        wx.showToast({
          title: '个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  //清除搜索内容
  clearSearchKeyword() {
    if (this.data.keyword) {
      this.setData({
        inputFocus: false,
        keyword: ''
      })
      this.setData({
        pageNum: 1,
        isLastPage: false
      })
      this.getData()
    }
    this.setData({
      inputFocus: false,
    })
  },
  searchFocus() {
    this.setData({
      inputFocus: true
    })
  },
  //搜索缘分
  searchFate(e) {
    this.setData({
      keyword: e.detail.value
    })
    //搜索数据
    this.setData({
      pageNum: 1,
      isLastPage: false
    })
    this.getData()
  },
  //显示隐藏筛选框
  selectRule() {
    this.setData({
      hiddenSelect: !this.data.hiddenSelect
    })
  },
  //切换导航
  switchNav(e) {
    this.setData({
      currentNavIdx: e.currentTarget.dataset.idx
    })
    if (this.data.currentNavIdx == 0) {
      this.setData({
        orderPageNum: 1,
        orderIsLastPage: false
      })
      this.getMyEnroll('refresh')
      return
    }
    this.setData({
      pageNum: 1,
      isLastPage: false
    })
    this.getData()
  },
  toDetail(e) {
    let sn = e.currentTarget.dataset.sn
    wx.navigateTo({
      url: '../../pages/activity-detail/activity-detail?sn=' + sn,
    })
  },
  toMemberDetail(e) {
    wx.navigateTo({
      url: '../../pages/member-detail/member-detail?id=' + e.currentTarget.dataset.id,
    })
  },

  /**
   * 筛选择偶条件
   * */

  //获取学历列表
  getEducationData() {
    var vm = this;
    app.api.getEducation()
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            education: '',
            desc: '不限',
            value: '100'
          })
          vm.setData({
            educationData: [res.data, res.data]
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '学历列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '学历列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取收入列表
  getIncomeData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'INCOME '
      })
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            id: '',
            name: '不限'
          })
          vm.setData({
            incomeData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取星座列表
  getStarSignData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'STAR_SIGN '
      })
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            id: '',
            name: '不限'
          })
          vm.setData({
            starSignData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '星座列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '星座列表请求出错了！',
          icon: 'none'
        })
      })
  },
  selectMarital(e) {
    let maritalState = 'rule.maritalState'
    this.setData({
      [maritalState]: e.currentTarget.dataset.maritalstate
    })
  },
  //选择学历
  bindEducationChange: function(e) {
    let educationData = this.data.educationData;
    let canSelectVal = []
    if (e.detail.column == 0) {
      for (let i = 0; i < educationData[0].length; i++) {
        if (educationData[0][i].value >= educationData[0][e.detail.value].value) {
          canSelectVal.push(educationData[0][i])
        }
      }
      educationData[1] = canSelectVal
    }
    this.setData({
      educationData: educationData
    })
  },
  //确认学历选择
  bindEducationSure(e) {
    let startEducation = 'rule.startEducation'
    let endEducation = 'rule.endEducation'

    let startEducationDesc = 'rule.startEducationDesc'
    let endEducationDesc = 'rule.endEducationDesc'

    let startIdx = e.detail.value[0];
    let endIdx = e.detail.value[1];
    this.setData({
      [startEducation]: this.data.educationData[0][startIdx].education,
      [endEducation]: this.data.educationData[1][endIdx].education,
      [startEducationDesc]: this.data.educationData[0][startIdx].desc,
      [endEducationDesc]: this.data.educationData[1][endIdx].desc,
    })
  },

  //选择收入
  bindIncomeChange(e) {
    let income = 'rule.income'
    this.setData({
      [income]: this.data.incomeData[e.detail.value].name
    })
  },
  //选择星座
  bindStarSignChange(e) {
    let starSign = 'rule.starSign'
    this.setData({
      [starSign]: this.data.starSignData[e.detail.value].name
    })
  },
  //选择年龄区间
  bindAgeChange: function(e) {
    this.setData({
      ageIdx: e.detail.value
    })
  },
  //选择身高区间
  bindHeightChange: function(e) {
    this.setData({
      heightIdx: e.detail.value
    })
  },
  onSelect(e) {
    if (this.data.selectAreaType == 1) {
      let domicileAreaId = 'rule.domicileAreaId';
      let domicileAreaName = 'rule.domicileAreaName'
      this.setData({
        [domicileAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [domicileAreaName]: e.detail.value.join('')
      })
    } else if (this.data.selectAreaType == 2) {
      let dwellingAreaId = 'rule.dwellingAreaId';
      let dwellingAreaName = 'rule.dwellingAreaName'
      this.setData({
        [dwellingAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [dwellingAreaName]: e.detail.value.join('')
      })
    }
  },
  //选择家乡
  selectDom() {
    this.setData({
      selectAreaType: 1
    })
  },
  //选择居住地
  selectDwe() {
    this.setData({
      selectAreaType: 2
    })
  },
  //清空规则
  clearRule() {
    this.setData({
      rule: {
        startEducation: '',
        endEducation: '',
        startEducationDesc: '',
        endEducationDesc: '',
        education: '',
        income: '',
        maritalState: '',
        starSign: '',
        domicileAreaId: '',
        domicileAreaName: '',
        dwellingAreaId: '',
        dwellingAreaName: '',
      },
      ageIdx: [],
      heightIdx: [],
    })
    app.globalData.writeUserInfo = {}
  },
  ruleSubmit() {
    this.setData({
      pageNum: 1,
      isLastPage: false,
    })
    this.getData()
    this.selectRule()
  }
})