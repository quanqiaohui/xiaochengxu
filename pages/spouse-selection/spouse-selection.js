const app = getApp()
const util = require('../../utils/util.js')
Page({
  data: {
    userInfo: {},
    educationData: [],
    incomeData: [],
    starSignData: [],
    maritalData: [{
      name: '单身',
      type: 'SINGLE'
    }, {
      name: '离异',
      type: 'DIVORCE'
    }, {
      name: '丧偶',
      type: 'WIDOWED'
    }],
    ageSectionArr: [],
    ageIdx: [],
    heightSectionArr: [],
    heightIdx: [],

    codes: [],
    city: '',
    citylist: [],
    selectAreaType: '', //选择地区类型 1、选择家乡 2、选择居住地

    educationIndex: [0, 0],
    incomeIdx: 0,
    starSignIdx: 0
  },
  onLoad: function(options) {
    let ageSection = [],
      ageSectionArr = [];
    for (let i = 18; i < 70; i++) {
      ageSection.push(i)
    }
    ageSectionArr.push(ageSection, ageSection)
    this.setData({
      ageSectionArr: ageSectionArr
    })
    let heightSection = [],
      heightSectionArr = [];
    for (let i = 148; i < 200; i++) {
      heightSection.push(i)
    }
    heightSectionArr.push(heightSection, heightSection)
    this.setData({
      heightSectionArr: heightSectionArr
    })
    this.getUserInfo()
    this.getAreas()
  },
  onSelect(e) {
    if (this.data.selectAreaType == 1) {
      let domicileAreaId = 'userInfo.domicileAreaId';
      let domicileAreaName = 'userInfo.domicileAreaName'
      this.setData({
        [domicileAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [domicileAreaName]: e.detail.value.join('')
      })
    } else if (this.data.selectAreaType == 2) {
      let dwellingAreaId = 'userInfo.dwellingAreaId';
      let dwellingAreaName = 'userInfo.dwellingAreaName'
      this.setData({
        [dwellingAreaId]: e.detail.code[e.detail.code.length - 1] ? e.detail.code[e.detail.code.length - 1] : e.detail.code[e.detail.code.length - 2] ? e.detail.code[e.detail.code.length - 2] : e.detail.code[e.detail.code.length - 3],
        [dwellingAreaName]: e.detail.value.join('')
      })
    }
  },
  //选择家乡
  selectDom() {
    let codes = []
    if (this.data.userInfo.domicileAreas && this.data.userInfo.domicileAreas.length > 0) {
      for (let i = 0; i < this.data.userInfo.domicileAreas.length; i++) {
        codes.push(this.data.userInfo.domicileAreas[i].id)
      }
    }
    this.setData({
      selectAreaType: 1,
      codes: codes
    })
  },
  //选择居住地
  selectDwe() {
    let codes = []
    if (this.data.userInfo.dwellingAreas && this.data.userInfo.dwellingAreas.length > 0) {
      for (let i = 0; i < this.data.userInfo.dwellingAreas.length; i++) {
        codes.push(this.data.userInfo.dwellingAreas[i].id)
      }
    }
    this.setData({
      selectAreaType: 2,
      codes: codes
    })
  },
  getAreas() {
    let params = {
        unlimited: true
      },
      vm = this;
    app.api.getAreas(params)
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            citylist: res.data.options
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '地址请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '地址请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取个人信息
  getUserInfo() {
    var vm = this;
    app.api.getUserInfo()
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            userInfo: res.data.singleIdeal ? res.data.singleIdeal : {}
          })
          let ageIdx = vm.data.ageIdx
          for (let i = 0; i < vm.data.ageSectionArr[0].length; i++) {
            if (vm.data.ageSectionArr[0][i] == vm.data.userInfo.startAge) {
              ageIdx[0] = i
            }
            if (vm.data.ageSectionArr[0][i] == vm.data.userInfo.endAge) {
              ageIdx[1] = i
            }
          }
          let ageSectionArr = vm.data.ageSectionArr;
          ageSectionArr[0][ageIdx[0]] = vm.data.userInfo.startAge
          ageSectionArr[1][ageIdx[1]] = vm.data.userInfo.endAge
          let ageSection = []
          if (ageIdx[0]) {
            for (let i = 18 + ageIdx[0]; i < 70; i++) {
              ageSection.push(i)
            }
          }
          vm.setData({
            ageIdx: [ageIdx[0], ageIdx[1] - ageIdx[0]],
            ageSectionArr: [ageSectionArr[0], ageSection],
          })

          let heightIdx = vm.data.heightIdx
          for (let i = 0; i < vm.data.heightSectionArr[0].length; i++) {
            if (vm.data.heightSectionArr[0][i] == vm.data.userInfo.startHeight) {
              heightIdx[0] = i
            }
            if (vm.data.heightSectionArr[0][i] == vm.data.userInfo.endHeight) {
              heightIdx[1] = i
            }
          }
          let heightSectionArr = vm.data.heightSectionArr;
          heightSectionArr[0][heightIdx[0]] = vm.data.userInfo.startHeight;
          heightSectionArr[1][heightIdx[1]] = vm.data.userInfo.endHeight;
          let heightSection = []
          if (heightIdx[0]) {
            for (let i = 148 + heightIdx[0]; i < 200; i++) {
              heightSection.push(i)
            }
          }
          vm.setData({
            heightIdx: [heightIdx[0], heightIdx[1] - heightIdx[0]],
            heightSectionArr: [heightSectionArr[0], heightSection],
          })
          vm.getEducationData()
          vm.getIncomeData()
          vm.getStarSignData()
        } else {
          wx.showToast({
            title: res.message ? res.message : '择偶标准请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '择偶标准请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取学历列表
  getEducationData() {
    var vm = this;
    app.api.getEducation()
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            education: '',
            desc: '不限',
            value: '100'
          })
          vm.setData({
            educationData: [res.data, res.data]
          })
          let educationIndex = vm.data.educationIndex
          for (let i = 0; i < vm.data.educationData[0].length; i++) {
            if (vm.data.educationData[0][i].education == vm.data.userInfo.startEducation) {
              educationIndex[0] = i
            }
            if (vm.data.educationData[0][i].education == vm.data.userInfo.endEducation) {
              educationIndex[1] = i
            }
          }
          vm.setData({
            educationIndex: educationIndex
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '学历列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '学历列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取收入列表
  getIncomeData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'INCOME '
      })
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            id: '',
            name: '不限'
          })
          vm.setData({
            incomeData: res.data
          })
          let incomeIdx = vm.data.incomeIdx
          for (let i = 0; i < vm.data.incomeData.length; i++) {
            if (vm.data.incomeData[i].name == vm.data.userInfo.income) {
              incomeIdx = i
            }
          }
          vm.setData({
            incomeIdx: incomeIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取星座列表
  getStarSignData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'STAR_SIGN '
      })
      .then(res => {
        if (res.code == 200) {
          res.data.unshift({
            id: '',
            name: '不限'
          })
          vm.setData({
            starSignData: res.data
          })
          let starSignIdx = vm.data.starSignIdx
          for (let i = 0; i < vm.data.starSignData.length; i++) {
            if (vm.data.starSignData[i].name == vm.data.userInfo.starSign) {
              starSignIdx = i
            }
          }
          vm.setData({
            starSignIdx: starSignIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '星座列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '星座列表请求出错了！',
          icon: 'none'
        })
      })
  },
  selectMarital(e) {
    let maritalState = 'userInfo.maritalState'
    // if (e.currentTarget.dataset.maritalstate == this.data.userInfo.maritalState) {
    //   this.setData({
    //     [maritalState]: ''
    //   })
    // } else {
    this.setData({
      [maritalState]: e.currentTarget.dataset.maritalstate
    })
    // }
  },
  //选择学历
  bindEducationChange: function(e) {
    let educationData = this.data.educationData;
    let canSelectVal = []
    if (e.detail.column == 0) {
      for (let i = 0; i < educationData[0].length; i++) {
        if (educationData[0][i].value >= educationData[0][e.detail.value].value) {
          canSelectVal.push(educationData[0][i])
        }
      }
      educationData[1] = canSelectVal
    }
    this.setData({
      educationData: educationData
    })
  },
  //确认学历选择
  bindEducationSure(e) {
    let startEducation = 'userInfo.startEducation'
    let endEducation = 'userInfo.endEducation'

    let startEducationDesc = 'userInfo.startEducationDesc'
    let endEducationDesc = 'userInfo.endEducationDesc'

    let startIdx = e.detail.value[0];
    let endIdx = e.detail.value[1];
    this.setData({
      [startEducation]: this.data.educationData[0][startIdx].education,
      [endEducation]: this.data.educationData[1][endIdx].education,
      [startEducationDesc]: this.data.educationData[0][startIdx].desc,
      [endEducationDesc]: this.data.educationData[1][endIdx].desc,
    })
  },
  //选择收入
  bindIncomeChange(e) {
    let income = 'userInfo.income'
    this.setData({
      [income]: this.data.incomeData[e.detail.value].name
    })
  },
  //选择星座
  bindStarSignChange(e) {
    let starSign = 'userInfo.starSign'
    this.setData({
      [starSign]: this.data.starSignData[e.detail.value].name
    })
  },
  //选择年龄区间
  bindAgeChange: function(e) {
    this.setData({
      ageIdx: e.detail.value
    })
  },
  //结束时间根据最小时间动态改变
  bindAgeColumnChange(e) {
    if (e.detail.column === 0) {
      let ageSectionArr1 = this.data.ageSectionArr[1];
      let ageSection = [],
        ageSectionArr = [];
      for (let i = this.data.ageSectionArr[0][e.detail.value]; i < 70; i++) {
        ageSection.push(i)
      }
      ageSectionArr.push(this.data.ageSectionArr[0], ageSection)
      this.setData({
        ageSectionArr: ageSectionArr
      })
    }
  },
  //选择身高区间
  bindHeightChange: function(e) {
    this.setData({
      heightIdx: e.detail.value
    })
  },
  //结束身高根据最小身高动态改变
  bindHeightColumnChange(e, initVal) {
    if (e.detail.column === 0) {
      let heightSectionArr1 = this.data.heightSectionArr[1];
      let heightSection = [],
        heightSectionArr = [];
      for (let i = this.data.heightSectionArr[0][e.detail.value]; i < 200; i++) {
        heightSection.push(i)
      }
      heightSectionArr.push(this.data.heightSectionArr[0], heightSection)
      this.setData({
        heightSectionArr: heightSectionArr,
      })
    }
  },
  //选择家乡
  selectDomicileAreas() {
    let domicileAreaIdArr = [];
    if (this.data.userInfo.domicileAreas && this.data.userInfo.domicileAreas.length > 0) {
      for (let i = 0; i < this.data.userInfo.domicileAreas.length; i++) {
        domicileAreaIdArr.push(this.data.userInfo.domicileAreas[i].id)
      }
    }
    wx.navigateTo({
      url: '../../pages/member-area/member-area?unlimited=true&setAreaType=2&domicileAreaIdArr=' + JSON.stringify(domicileAreaIdArr),
    })
  },
  //选择居住地
  selectDwellingAreas() {
    let dwellingAreaIdArr = [];
    if (this.data.userInfo.dwellingAreas && this.data.userInfo.dwellingAreas.length > 0) {
      for (let i = 0; i < this.data.userInfo.dwellingAreas.length; i++) {
        dwellingAreaIdArr.push(this.data.userInfo.dwellingAreas[i].id)
      }
    }
    wx.navigateTo({
      url: '../../pages/member-area/member-area?unlimited=true&setAreaType=1&dwellingAreaIdArr=' + JSON.stringify(dwellingAreaIdArr),
    })
  },
  submit() {
    let userInfo = this.data.userInfo;
    let params = {
      startAge: this.data.ageSectionArr[0][this.data.ageIdx[0]],
      endAge: this.data.ageSectionArr[1][this.data.ageIdx[1]],
      startHeight: this.data.heightSectionArr[0][this.data.heightIdx[0]],
      endHeight: this.data.heightSectionArr[1][this.data.heightIdx[1]],
      startEducation: userInfo.startEducation,
      endEducation: userInfo.endEducation,
      income: userInfo.income,
      maritalState: userInfo.maritalState,
      starSign: userInfo.starSign,
      domicileAreaId: userInfo.domicileAreaId,
      dwellingAreaId: userInfo.dwellingAreaId,
    }
    app.api.updateChooseIdeals(params)
      .then(res => {
        if (res.code == 200) {
          app.globalData.writeUserInfo = {}
          wx.navigateBack({
            delta: 1
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '更新择偶标准请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '更新择偶标准请求出错了！',
          icon: 'none'
        })
      })
  }
})