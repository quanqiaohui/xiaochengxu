const app = getApp()
Page({
  data: {
    provinces: [],
    province: {},
    provinceId: '',
    citys: [],
    city: {},
    cityId: '',
    areas: [],
    area: {},
    areaId: '',
    value: [1, 1, 1],
    currentIdx: 1,
    selectResult: [0, 0, 0],
    dwellingAreaIdx: [],
    dwellingAreaIdArr: [],
    setAreaType: '', //设置地区类型 1、0工作地区 2家乡
    unlimited: false
  },
  bindChange: function(e) {
    let dwellingAreaIdx = this.data.dwellingAreaIdx;
    const val = e.detail.value
    if (this.data.selectResult[0] != val[0]) {
      //滑动省
      this.setData({
        currentIdx: 2,
      })
      if (this.data.provinces[val[0]].id) {
        dwellingAreaIdx[0] = val[0]
        this.getArea(this.data.provinces[val[0]].id, this.data.provinces[val[0]].name, 'onLoad')
      } else {
        this.setData({
          provinceId: '',

          citys: [],
          city: {},
          value: [0, 0, 0],
          cityId: '',

          areas: [],
          area: {},
          areaId: '',
          selectResult: [0, 0, 0]
        })
        return
      }
    }
    if (this.data.selectResult[1] != e.detail.value[1]) {
      //滑动市
      this.setData({
        currentIdx: 3
      })
      dwellingAreaIdx[1] = val[1]
      this.getArea(this.data.citys[val[1]].id, this.data.citys[val[1]].name)
    }
    if (this.data.selectResult[2] != e.detail.value[2]) {
      //滑动区
      dwellingAreaIdx[2] = val[2]
    }
    this.setData({
      province: this.data.provinces[val[0]],
      provinceId: this.data.provinces[val[0]].id,
      city: this.data.citys[val[1]],
      cityId: this.data.citys[val[1]] ? this.data.citys[val[1]].id : '',
      area: this.data.areas && this.data.areas.length > 2 ? this.data.areas[val[2]] : {},
      areaId: this.data.areas && this.data.areas.length > 2 ? this.data.areas[val[2]].id : '',
      selectResult: e.detail.value,
      dwellingAreaIdx: dwellingAreaIdx
    })
  },
  onLoad(options) {
    if (options && options.setAreaType) {
      this.setData({
        setAreaType: options.setAreaType
      })
    }
    if (options && options.unlimited) {
      this.setData({
        unlimited: true
      })
    }
    if (options && options.dwellingAreaIdArr && JSON.parse(options.dwellingAreaIdArr).length > 0 && options.setAreaType) {
      this.setData({
        dwellingAreaIdArr: JSON.parse(options.dwellingAreaIdArr)
      })
      this.getDefArea(1)
    } else if (options && options.domicileAreaIdArr && JSON.parse(options.domicileAreaIdArr).length > 0 && options.setAreaType) {
      this.setData({
        dwellingAreaIdArr: JSON.parse(options.domicileAreaIdArr)
      })
      this.getDefArea(1)
    } else if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').dwellingAreaId && !options.setAreaType) {
      this.setData({
        dwellingAreaIdArr: wx.getStorageSync('writeUserInfo').dwellingAreaIdArr
      })
      this.getDefArea(1)
    } else {
      this.getArea('', '', 'onLoad')
    }
  },
  getArea(parentId, parentName, onLoad) {
    let dwellingAreaIdx = this.data.dwellingAreaIdx;
    var vm = this;
    var data = {};
    if (this.data.currentIdx == 2 || this.data.currentIdx == 3) {
      data.parentId = parentId;
      data.name = parentName;
    }
    if (this.data.unlimited) {
      data.unlimited = true
    }

    app.api.getArea(data)
      .then(res => {
        if (res.code == 200) {
          if (vm.data.currentIdx == 1) {
            vm.setData({
              provinces: res.data,
              province: res.data && res.data.length > 0 ? res.data[0] : {},
              provinceId: res.data[0].id
            })
            dwellingAreaIdx[0] = 0;
            if (onLoad = 'onLoad') {
              vm.setData({
                currentIdx: 2
              })
              vm.getArea(res.data[0].id, res.data[0].name, 'onLoad')
            }
          } else if (vm.data.currentIdx == 2) {
            vm.setData({
              citys: res.data,
              city: res.data && res.data.length > 0 ? res.data[0] : {},
              value: [vm.data.selectResult[0], 0, 0],
              cityId: res.data && res.data.length > 0 ? res.data[0].id : ''
            })
            dwellingAreaIdx[1] = 0;
            if (onLoad = 'onLoad') {
              vm.setData({
                currentIdx: 3
              })
              vm.getArea(res.data[0].id, res.data[0].name, 'onLoad')
            }
          } else if (vm.data.currentIdx == 3) {
            dwellingAreaIdx[2] = 0;
            vm.setData({
              areas: res.data,
              area: res.data && res.data.length > 0 ? res.data[0] : {},
              areaId: res.data && res.data.length > 0 ? res.data[0].id : ''
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '地址请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '地址请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取默认地址
  getDefArea(index, parentId) {
    var vm = this;
    var data = {};
    let dwellingAreaIdx = vm.data.dwellingAreaIdx;
    if (parentId) {
      data.parentId = parentId;
    }
    let dwellingAreaIdArr = this.data.dwellingAreaIdArr;
    if (this.data.unlimited) {
      data.unlimited = true
    }

    app.api.getArea(data)
      .then(res => {
        if (res.code == 200) {
          if (index == 1) {
            vm.setData({
              provinces: res.data,
              province: res.data && res.data.length > 0 ? res.data[0] : {}
            })

            for (let i = 0; i < vm.data.provinces.length; i++) {
              if (vm.data.provinces[i].id == dwellingAreaIdArr[0]) {
                dwellingAreaIdx[0] = i
              }
            }
            vm.setData({
              dwellingAreaIdx: dwellingAreaIdx
            })
            vm.getDefArea(2, dwellingAreaIdArr[0])
          } else if (index == 2) {
            vm.setData({
              citys: res.data,
              city: res.data && res.data.length > 0 ? res.data[0] : {}
            })
            for (let i = 0; i < vm.data.citys.length; i++) {
              if (vm.data.citys[i].id == dwellingAreaIdArr[1]) {
                dwellingAreaIdx[1] = i
              }
            }
            vm.setData({
              dwellingAreaIdx: dwellingAreaIdx
            })
            vm.getDefArea(3, dwellingAreaIdArr[1])
          } else if (index == 3) {
            vm.setData({
              areas: res.data,
              area: res.data && res.data.length > 0 ? res.data[0] : {}
            })
            for (let i = 0; i < vm.data.areas.length; i++) {
              if (vm.data.areas[i].id == dwellingAreaIdArr[2]) {
                dwellingAreaIdx[2] = i
              }
            }
            vm.setData({
              dwellingAreaIdx: dwellingAreaIdx
            })
          }
          vm.setData({
            value: dwellingAreaIdx,
            province: vm.data.provinces[dwellingAreaIdx[0]],
            provinceId: vm.data.provinces[dwellingAreaIdx[0]].id,
            city: vm.data.citys && vm.data.citys.length > 0 ? vm.data.citys[dwellingAreaIdx[1]] : {},
            cityId: vm.data.citys && vm.data.citys.length > 0 && vm.data.citys[dwellingAreaIdx[1]] ? vm.data.citys[dwellingAreaIdx[1]].id : '',
            area: vm.data.areas && vm.data.areas.length > 0 ? vm.data.areas[dwellingAreaIdx[2]] : {},
            areaId: vm.data.areas && vm.data.areas.length > 0 && vm.data.areas[dwellingAreaIdx[2]] ? vm.data.areas[dwellingAreaIdx[2]].id : '',
            selectResult: dwellingAreaIdx
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '地址请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '地址请求出错了！',
          icon: 'none'
        })
      })
  },
  next() {
    let dwellingAreaId = ''
    if (this.data.areaId) {
      dwellingAreaId = this.data.areaId;
    } else if (this.data.cityId) {
      dwellingAreaId = this.data.cityId;
    } else if (this.data.provinceId) {
      dwellingAreaId = this.data.provinceId;
    }
    if (this.data.setAreaType) {
      this.update(dwellingAreaId)
    } else {
      let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
      writeUserInfo.dwellingAreaId = dwellingAreaId;
      writeUserInfo.dwellingAreaIdArr = [this.data.provinceId, this.data.cityId, this.data.areaId];
      wx.setStorageSync('writeUserInfo', writeUserInfo);
      wx.redirectTo({
        url: '../../pages/member-photos/member-photos',
      })
    }
  },
  //完成修改家乡/居住地
  update(id) {
    let params = {}
    let writeUserInfo = app.globalData.writeUserInfo;

    if (this.data.setAreaType == 1) {
      writeUserInfo.dwellingAreaId = id;
      writeUserInfo.dwellingAreas = [];
      if (this.data.provinceId) {
        writeUserInfo.dwellingAreas[0] = {};
        writeUserInfo.dwellingAreas[0].id = this.data.provinceId;
        writeUserInfo.dwellingAreas[0].name = this.data.provinces[this.data.dwellingAreaIdx[0]].name;
        writeUserInfo.dwellingAreas[0].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name;
      }
      if (this.data.cityId) {
        writeUserInfo.dwellingAreas[1] = {};
        writeUserInfo.dwellingAreas[1].id = this.data.cityId;
        writeUserInfo.dwellingAreas[1].name = this.data.citys[this.data.dwellingAreaIdx[1]].name;
        writeUserInfo.dwellingAreas[1].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name + this.data.citys[this.data.dwellingAreaIdx[1]].name;
      }
      if (this.data.areaId) {
        writeUserInfo.dwellingAreas[2] = {};
        writeUserInfo.dwellingAreas[2].id = this.data.areaId;
        writeUserInfo.dwellingAreas[2].name = this.data.areas[this.data.dwellingAreaIdx[2]].name;
        writeUserInfo.dwellingAreas[2].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name + this.data.citys[this.data.dwellingAreaIdx[1]].name + this.data.areas[this.data.dwellingAreaIdx[2]].name;
      }
      if (!this.data.provinceId) {
        writeUserInfo.dwellingAreaId = '';
        writeUserInfo.dwellingAreas = [{
          fullName: '不限',
          id: ''
        }];
      }
    } else if (this.data.setAreaType == 2) {
      writeUserInfo.domicileAreaId = id;
      writeUserInfo.domicileAreas = [];
      if (this.data.provinceId) {
        writeUserInfo.domicileAreas[0] = {};
        writeUserInfo.domicileAreas[0].id = this.data.provinceId;
        writeUserInfo.domicileAreas[0].name = this.data.provinces[this.data.dwellingAreaIdx[0]].name;
        writeUserInfo.domicileAreas[0].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name;
      }
      if (this.data.cityId) {
        writeUserInfo.domicileAreas[1] = {};
        writeUserInfo.domicileAreas[1].id = this.data.cityId;
        writeUserInfo.domicileAreas[1].name = this.data.citys[this.data.dwellingAreaIdx[1]].name;
        writeUserInfo.domicileAreas[1].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name + this.data.citys[this.data.dwellingAreaIdx[1]].name;
      }
      if (this.data.areaId) {
        writeUserInfo.domicileAreas[2] = {};
        writeUserInfo.domicileAreas[2].id = this.data.areaId;
        writeUserInfo.domicileAreas[2].name = this.data.areas[this.data.dwellingAreaIdx[2]].name;
        writeUserInfo.domicileAreas[2].fullName = this.data.provinces[this.data.dwellingAreaIdx[0]].name + this.data.citys[this.data.dwellingAreaIdx[1]].name + this.data.areas[this.data.dwellingAreaIdx[2]].name;
      }
      if (!this.data.provinceId) {
        writeUserInfo.domicileAreaId = '';
        writeUserInfo.domicileAreas = [{
          fullName: '不限',
          id: ''
        }];
      }
    }
    wx.navigateBack({
      delta: 1
    })
  }
})