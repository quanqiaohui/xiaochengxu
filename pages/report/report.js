// pages/userinfo.js
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    checked: false,
    photoImg: [],
    reportData: {
      "accusedSingleId": '',
      "contact": '',
      "fileInfoIds": [],
      "mobile": "",
      "name": "",
      "reason": "",
      "reasonType": "",
      "singleMobile": "",
      "wechat": ""
    },
    reasonList: [],
    reportUserInfo: {}
  },
  onLoad: function(options) {
    let accusedSingleId = 'reportData.accusedSingleId'
    this.setData({
      reportUserInfo: JSON.parse(options.reportUserInfo),
      [accusedSingleId]: JSON.parse(options.reportUserInfo).id
    })
    this.getReportData()
  },
  //真实姓名
  changeName: function(e) {
    let name = 'reportData.name'
    this.setData({
      [name]: e.detail.value
    })
  },
  //电话号码
  changeMobile: function(e) {
    let mobile = 'reportData.mobile'
    this.setData({
      [mobile]: e.detail.value
    })
  },
  //微信号
  changeWechat: function(e) {
    let wechat = 'reportData.wechat'
    this.setData({
      [wechat]: e.detail.value
    })
  },
  //其他联系方式
  changeContact: function(e) {
    let contact = 'reportData.contact'
    this.setData({
      [contact]: e.detail.value
    })
  },
  //获取举报原因字典
  getReportData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'ACCUSE_REASON_TYPE '
      })
      .then(res => {
        if (res.code == 200) {
          if (res.data) {
            vm.setData({
              reasonList: res.data
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '举报原因列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '举报原因列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //举报原因
  bindPickerChange: function(e) {
    let reasonType = 'reportData.reasonType'
    this.setData({
      [reasonType]: this.data.reasonList[e.detail.value].name
    })
  },
  //具体描述
  changeReason: function(e) {
    let reason = 'reportData.reason'
    this.setData({
      [reason]: e.detail.value
    })
  },
  //自己电话
  changeSingleMobile: function(e) {
    let singleMobile = 'reportData.singleMobile'
    this.setData({
      [singleMobile]: e.detail.value
    })
  },
  //同意免责条款
  changeCheck: function() {
    var that = this;
    this.setData({
      checked: !that.data.checked
    })
  },
  uploadImg() {
    let vm = this;
    wx.chooseImage({
      count: 5 - this.data.photoImg.length,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        var successUp = 0; //成功
        var failUp = 0; //失败
        var length = res.tempFilePaths.length; //总数
        var count = 0; //第几张
        vm.uploadOneByOne(res.tempFilePaths, successUp, failUp, count, length);
      }
    })
  },
  /**
   * 采用递归的方式上传多张
   */
  uploadOneByOne(imgPaths, successUp, failUp, count, length) {
    var vm = this;
    wx.showLoading({
      title: '正在上传第' + count + '张',
    })
    wx.uploadFile({
      url: `${app.api._baseUrl}/api/user/file/batchUpload?access_token=${wx.getStorageSync('accessToken')}`,
      filePath: imgPaths[count],
      name: 'file',
      header: {
        "Content-Type": "multipart/form-data"
      },
      success: function(res) {
        let data = JSON.parse(res.data).data;
        successUp++; //成功+1
        let photoImg = []
        if (vm.data.photoImg && vm.data.photoImg.length > 0) {
          photoImg = vm.data.photoImg.concat(data)
        } else {
          photoImg = data
        }
        vm.setData({
          photoImg: photoImg
        })
      },
      fail: function(e) {
        failUp++; //失败+1
      },
      complete: function(e) {
        count++; //下一张
        if (count == length) {
          //上传完毕，作一下提示
          // console.log('上传成功' + successUp + ',' + '失败' + failUp);
          wx.showToast({
            title: '上传成功' + successUp + '张',
            icon: 'success',
            duration: 2000
          })
        } else {
          //递归调用，上传下一张
          vm.uploadOneByOne(imgPaths, successUp, failUp, count, length);
          // console.log('正在上传第' + count + '张');
        }
      }
    })
  },
  //删除图片
  delPhoto(e) {
    let vm = this,
      id = e.currentTarget.dataset.id,
      photoImg = vm.data.photoImg,
      index = e.currentTarget.dataset.index;
    wx.showActionSheet({
      itemList: ['刪除照片'],
      success(res) {
        if (res.tapIndex === 0) {
          photoImg.splice(index, 1);
        }
      }
    })
  },
  previewPhotos(e) {
    let urls = [];
    for (let i = 0; i < this.data.photoImg.length; i++) {
      urls.push(this.data.photoImg[i].url)
    }
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: urls
    })
  },
  submit() {
    if (!this.data.checked) {
      wx.showToast({
        title: '请选阅读并同意举报相关信息的免责条款',
        icon: 'none'
      })
      return
    }
    if (!this.data.reportData.reasonType){
      wx.showToast({
        title: '请选择举报原因',
        icon: 'none'
      })
      return
    }
    if (!this.data.reportData.reason) {
      wx.showToast({
        title: '请填写具体描述',
        icon: 'none'
      })
      return
    }
    let params = this.data.reportData;
    if (this.data.photoImg && this.data.photoImg.length > 0) {
      for (let i = 0; i < this.data.photoImg.length; i++) {
        params.fileInfoIds.push(this.data.photoImg[i].id)
      }
    }
    let vm = this;
    app.api.report(params)
      .then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '举报成功',
          })
          wx.navigateBack({
            delta: -1
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '举报请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '举报请求出错了！',
          icon: 'none'
        })
      })
  }
})