const app = getApp()
Page({
  data: {
    introduce: '',
    listData: []
  },
  onLoad: function(options) {
    this.setData({
      introduce: options.introduce && options.introduce != 'null' ? options.introduce : ''
    })
    this.getData()
  },
  //获取数据列表
  getData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'INTRODUCE_EXAMPLE '
      })
      .then(res => {
        if (res.code == 200) {
          if (res.data && res.data.length > 0) {
            for (let i = 0; i < res.data.length; i++) {
              res.data[i].open = false;
            }
            vm.setData({
              listData: res.data
            })
          }
        }
      }).catch(res => {})
  },
  changeIntroduce(e) {
    this.setData({
      introduce: e.detail.value
    })
  },
  useExample(e){
    this.setData({
      introduce: e.currentTarget.dataset.name
    })
  },
  checkExample(e) {
    let index = e.currentTarget.dataset.index;
    let listData = this.data.listData;
    if (listData[index].open) {
      listData[index].open = false;
    } else {
      listData[index].open = true;
    }
    this.setData({
      listData: listData
    })
  },
  submit() {
    let params = {
      "introduce": this.data.introduce
    }
    app.api.updateUserInfo(params)
      .then(res => {
        if (res.code == 200) {
          wx.navigateBack({
            delta: 1
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '更新个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '更新个人信息请求出错了！',
          icon: 'none'
        })
      })
  }
})