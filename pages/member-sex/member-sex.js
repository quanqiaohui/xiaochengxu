const app = getApp();
Page({
  data: {
    sex: 'MALE'
  },
  onLoad: function(options) {
    if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').gender) {
      this.setData({
        sex: wx.getStorageSync('writeUserInfo').gender
      })
    }
  },
  onShow() {
  },
  selectSex(e) {
    this.setData({
      sex: e.currentTarget.dataset.sex
    })
  },
  save() {
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.gender = this.data.sex;
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-wechat/member-wechat',
    })
  }
})