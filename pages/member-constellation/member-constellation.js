const app = getApp();
Page({
  data: {
    listData: [],
    starSign: ''
  },
  onLoad: function (options) {
    this.getData()
  },
  //获取数据列表
  getData() {
    var vm = this;
    app.api.getDictItem({
      typeCode: 'STAR_SIGN '
    })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            listData: res.data
          })
          if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').starSign) {
            vm.setData({
              starSign: wx.getStorageSync('writeUserInfo').starSign
            })
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '星座列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '星座列表请求出错了！',
          icon: 'none'
        })
      })
  },
  changeSelect(e) {
    this.setData({
      starSign: e.currentTarget.dataset.name
    })
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.starSign = this.data.starSign;
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-area/member-area',
    })
  }
})