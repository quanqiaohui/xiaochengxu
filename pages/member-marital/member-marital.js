const app = getApp();
Page({
  data: {
    listData: [{
      name: '单身',
      type: 'SINGLE'
    }, {
      name: '离异',
      type: 'DIVORCE'
    }, {
      name: '丧偶',
      type: 'WIDOWED'
    }],
    maritalState: '',
  },
  onLoad: function(options) {
    if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').maritalState) {
      this.setData({
        maritalState: wx.getStorageSync('writeUserInfo').maritalState
      })
    }
  },
  changeSelect(e) {
    this.setData({
      maritalState: e.currentTarget.dataset.type
    })
    let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
    writeUserInfo.maritalState = this.data.maritalState;
    wx.setStorageSync('writeUserInfo', writeUserInfo);
    wx.navigateTo({
      url: '../../pages/member-constellation/member-constellation',
    })
  }
})