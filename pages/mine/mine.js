// pages/mine/mine.js

//获取应用实例
const app = getApp();

Page({
  data: {
    photoImg: [], //显示相册内容
    uploadImg: [], //当前上传图片
    userInfo: {},
    showModal: false,
    serviceWechat: '',
  },
  onLoad: function(options) {
    app.getIsHaveUserinfo('mine');
    this.getServiceWechat()
  },
  onShow: function() {
    app.globalData.writeUserInfo = {};
    if (app.globalData.addAvatarImgUrl) {
      this.updateAvatar()
    }
    this.getUserInfo();
  },
  onPullDownRefresh() {
    app.getIsHaveUserinfo('mine');
    this.getUserInfo('refresh')
  },
  //获取个人信息
  getUserInfo(refresh) {
    var vm = this;
    app.api.getUserInfo()
      .then(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        if (res.code == 200) {
          vm.setData({
            photoImg: res.data.singlePhotoDtoList,
            userInfo: res.data
          })
          vm.createProgressCanvas();
        } else {
          wx.showToast({
            title: res.message ? res.message : '个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        wx.showToast({
          title: '个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  // 创建进度条canvas
  createProgressCanvas() {
    var vm = this;
    const ctx2 = wx.createCanvasContext('runCanvas');
    wx.createSelectorQuery().select('#runCanvas').boundingClientRect(function(rect) { //监听canvas的宽高
      //获取canvas宽的的一半
      var w = parseInt(rect.width / 2);
      //获取canvas高的一半，
      var h = parseInt(rect.height / 2);
      vm.run(vm.data.userInfo.dataPercent, w, h, ctx2)
    }).exec();
  },
  run(c, w, h, ctx2) { //c是圆环进度百分比   w，h是圆心的坐标
    let vm = this;
    var num = (2 * Math.PI / 100 * c) - 0.5 * Math.PI;
    //圆环的绘制
    ctx2.arc(w, h, w - 8, -0.5 * Math.PI, num); //绘制的动作
    ctx2.setStrokeStyle("#EC6669"); //圆环线条的颜色
    ctx2.setLineWidth("2"); //圆环的粗细
    ctx2.setLineCap("butt"); //圆环结束断点的样式  butt为平直边缘 round为圆形线帽  square为正方形线帽
    ctx2.stroke();
    //开始绘制百分比数字
    ctx2.beginPath();
    ctx2.setFontSize(14); // 字体大小 注意不要加引号
    ctx2.setFillStyle("#EC6669"); // 字体颜色
    ctx2.setTextAlign("center"); // 字体位置
    ctx2.setTextBaseline("middle"); // 字体对齐方式
    ctx2.fillText(c + "%", w, h); // 文字内容和文字坐标
    ctx2.draw();
  },
  addPhoto(e) {
    let setType = e.currentTarget.dataset.type
    wx.navigateTo({
      url: '../../pages/cropper/cropper?setType=' + setType,
    })
  },
  //更新头像
  updateAvatar() {
    var vm = this;
    wx.showLoading({
      title: '上传头像',
    })
    app.api.updateAvatar({
        avatar: app.globalData.addAvatarImgUrl
      })
      .then(res => {
        wx.hideLoading();
        if (res.code == 200) {
          let avatar = "userInfo.avatar"
          vm.setData({
            [avatar]: app.globalData.addAvatarImgUrl
          });
          app.globalData.addAvatarImgUrl = '';
        } else {
          if (res.message) {
            wx.showToast({
              title: res.data.message ? res.data.message : '头像更新失败',
              icon: 'none',
              duration: 2000
            })
          }
        }
      }).catch(res => {
        wx.hideLoading();
        wx.showToast({
          title: '头像更新失败',
          icon: 'none',
          duration: 2000
        })
      })
  },
  uploadImg() {
    this.setData({
      uploadImg: []
    })
    let vm = this;
    wx.chooseImage({
      count: 6 - this.data.photoImg.length,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        var successUp = 0; //成功
        var failUp = 0; //失败
        var length = res.tempFilePaths.length; //总数
        var count = 0; //第几张
        vm.uploadOneByOne(res.tempFilePaths, successUp, failUp, count, length);
      }
    })
  },
  /**
   * 采用递归的方式上传多张
   */
  uploadOneByOne(imgPaths, successUp, failUp, count, length) {
    var vm = this;
    wx.showLoading({
      title: '正在上传第' + count + '张',
    })
    wx.uploadFile({
      url: `${app.api._baseUrl}/api/user/file/batchUpload?access_token=${wx.getStorageSync('accessToken')}`,
      filePath: imgPaths[count],
      name: 'file',
      header: {
        "Content-Type": "multipart/form-data"
      },
      success: function(res) {
        let data = JSON.parse(res.data).data;
        successUp++; //成功+1
        let uploadImg = []
        if (vm.data.uploadImg && vm.data.uploadImg.length > 0) {
          uploadImg = vm.data.uploadImg.concat(data)
        } else {
          uploadImg = data
        }
        vm.setData({
          uploadImg: uploadImg
        })
      },
      fail: function(e) {
        failUp++; //失败+1
      },
      complete: function(e) {
        count++; //下一张
        if (count == length) {
          //上传完毕，作一下提示
          // console.log('上传成功' + successUp + ',' + '失败' + failUp);
          vm.setData({
            successUp: successUp
          })
          vm.uploadPhotos()
        } else {
          //递归调用，上传下一张
          vm.uploadOneByOne(imgPaths, successUp, failUp, count, length);
          // console.log('正在上传第' + count + '张');
        }
      }
    })
  },
  // 上传相册
  uploadPhotos() {
    var vm = this;
    let fileInfoIds = [];
    let uploadImg = vm.data.uploadImg;
    for (let i = 0; i < uploadImg.length; i++) {
      uploadImg[i].fileInfoId = uploadImg[i].id;
      fileInfoIds.push(uploadImg[i].id)
    }
    if (vm.data.photoImg && vm.data.photoImg.length > 0) {
      for (let i = 0; i < vm.data.photoImg.length; i++) {
        fileInfoIds.push(vm.data.photoImg[i].fileInfoId)
      }
      uploadImg = vm.data.photoImg.concat(uploadImg)
    }
    app.api.uploadPhotos('/api/user/single/photos', {
        fileInfoIds: fileInfoIds
      })
      .then(res => {
        // 根据openId去登录（加密后的openId）
        if (res.code == 200) {
          vm.setData({
            photoImg: uploadImg
          });
          wx.showToast({
            title: '上传成功' + vm.data.successUp + '张',
            icon: 'success',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '上传相册失败',
            icon: 'none',
            duration: 2000
          })
        }
        wx.hideLoading();
      }).catch(res => {
        wx.hideLoading();
        wx.showToast({
          title: '上传相册失败',
          icon: 'none',
          duration: 2000
        })
      })
  },
  //删除/查看相册大图
  operatePhoto(e) {
    let vm = this;
    wx.showActionSheet({
      itemList: ['查看大图', '刪除'],
      success(res) {
        if (res.tapIndex === 0) {
          vm.previewPhotos(e)
        }
        if (res.tapIndex === 1) {
          vm.delPhoto(e)
        }
      }
    })
  },
  //删除图片
  delPhoto(e) {
    let vm = this,
      id = e.currentTarget.dataset.id,
      photoImg = vm.data.photoImg,
      index = e.currentTarget.dataset.index;
    app.api.delPhotosItem('/api/user/single/photos/' + id)
      .then(res => {
        if (res.code == 200) {
          photoImg.splice(index, 1);
          vm.setData({
            photoImg: photoImg,
          });
        } else {
          if (res.data.message) {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              duration: 2000
            })
          }
        }
      }).catch(res => {
        wx.showToast({
          title: '删除失败',
          icon: 'none',
          duration: 2000
        })
      })
  },
  previewPhotos(e) {
    let urls = [];
    for (let i = 0; i < this.data.photoImg.length; i++) {
      urls.push(this.data.photoImg[i].url)
    }
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: urls
    })
  },
  editUserInfo() {
    wx.navigateTo({
      url: '../../pages/edit-member/edit-member',
    })
  },
  editIntroduce() {
    wx.navigateTo({
      url: '../../pages/member-introduce/member-introduce?introduce=' + this.data.userInfo.introduce,
    })
  },
  editSpouseSelection() {
    wx.navigateTo({
      url: '../../pages/spouse-selection/spouse-selection',
    })
  },
  toFollow() {
    app.globalData.matchIdx = 2;
    wx.switchTab({
      url: '../../pages/match/match',
    })
  },
  //个人详情
  toMemberDetail(e) {
    wx.navigateTo({
      url: '../../pages/member-detail/member-detail?id=' + e.currentTarget.dataset.id,
    })
  },
  getServiceWechat() {
    let vm = this;
    app.api.getServiceWechat().then(res => {
      if (res.code == app.globalData.ok) {
        if (res.data) {
          vm.setData({
            serviceWechat: res.data
          })
        }
      } else {}
    }).catch(res => {})
  },
  // 打开弹窗
  openModal: function() {
    this.setData({
      showModal: true
    })
  },
  // 禁止屏幕滚动
  preventTouchMove: function() {},
  // 关闭弹窗
  closeModal: function() {
    this.setData({
      showModal: false
    })
  },
  copyWechat() {
    let vm = this;
    if (vm.data.serviceWechat) {
      wx.setClipboardData({
        data: vm.data.serviceWechat,
        success(res) {
          wx.getClipboardData({
            success(res) {}
          })
        }
      })
    } else {
      wx.showToast({
        title: '暂无填写微信号',
        icon: 'none'
      })
    }
  },
  toShare() {
    wx.navigateTo({
      url: '../../pages/business-card/business-card?memberId=' + this.data.userInfo.id,
    })
  },
  getWallUrl() {
    let vm = this;
    app.api.getWallUrl().then(res => {
      if (res.code == app.globalData.ok) {
        if (res.data) {
          wx.navigateTo({
            url: '../../pages/rule/rule?title=上墙须知&wallUrl=' + encodeURIComponent(res.data),
          })
        }
      } else {
        wx.showToast({
          title: res.message ? res.message : '上墙请求出错了！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.showToast({
        title: '上墙请求出错了！',
        icon: 'none'
      })
    })
  },
  toWall() {
    wx.navigateTo({
      url: '../../pages/upper-wall/upper-wall?wall=' + this.data.userInfo.wall,
    })
  }
})