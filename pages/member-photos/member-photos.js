const app = getApp()
Page({
  data: {
    listData: []
  },
  onLoad: function(options) {
    this.getPhotos()
  },
  changeSelect(e) {
    this.setData({
      currentIdx: e.currentTarget.dataset.index
    })
  },
  getPhotos() {
    var vm = this;
    app.api.getPhotos()
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            listData: res.data,
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '生活照列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '生活照列表请求出错了！',
          icon: 'none'
        })
      })
  },
  uploadImg() {
    let vm = this;
    wx.chooseImage({
      count: 6 - this.data.listData.length,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        var successUp = 0; //成功
        var failUp = 0; //失败
        var length = res.tempFilePaths.length; //总数
        var count = 0; //第几张
        vm.uploadOneByOne(res.tempFilePaths, successUp, failUp, count, length);
      }
    })
  },
  /**
   * 采用递归的方式上传多张
   */
  uploadOneByOne(imgPaths, successUp, failUp, count, length) {
    var that = this;
    wx.showLoading({
      title: '正在上传第' + count + '张',
    })
    wx.uploadFile({
      url: `${app.api._baseUrl}/api/user/file/batchUpload?access_token=${wx.getStorageSync('accessToken')}`,
      filePath: imgPaths[count],
      name: 'file',
      header: {
        "Content-Type": "multipart/form-data"
      },
      success: function(res) {
        let data = JSON.parse(res.data).data;
        successUp++; //成功+1
        let listData = []
        if (that.data.listData && that.data.listData.length > 0) {
          listData = that.data.listData.concat(data)
        } else {
          listData = data
        }
        that.setData({
          listData: listData
        })
      },
      fail: function(e) {
        failUp++; //失败+1
      },
      complete: function(e) {
        count++; //下一张
        if (count == length) {
          //上传完毕，作一下提示
          // console.log('上传成功' + successUp + ',' + '失败' + failUp);
          wx.showToast({
            title: '上传成功' + successUp + '张',
            icon: 'success',
            duration: 2000
          })
        } else {
          //递归调用，上传下一张
          that.uploadOneByOne(imgPaths, successUp, failUp, count, length);
          // console.log('正在上传第' + count + '张');
        }
      }
    })
  },
  delPhoto(e) {
    let imgArr = this.data.listData,
      vm = this;
    wx.showActionSheet({
      itemList: ['刪除照片'],
      success(res) {
        console.log(res.tapIndex)
        if (res.tapIndex === 0) {
          imgArr.splice(e.currentTarget.dataset.index, 1)
          vm.setData({
            listData: imgArr
          })
        }
      }
    })
  },
  save() {
    let fileInfoIds = [],
      writeUserInfo = wx.getStorageSync('writeUserInfo');
    for (let i = 0; i < this.data.listData.length; i++) {
      fileInfoIds.push(this.data.listData[i].id)
    }
    let params = {
      "wechat": writeUserInfo.wechat,
      "birth": writeUserInfo.birth,
      "dwellingAreaId": writeUserInfo.dwellingAreaId,
      "education": writeUserInfo.education,
      "gender": writeUserInfo.gender,
      "height": writeUserInfo.height,
      "income": writeUserInfo.income,
      "maritalState": writeUserInfo.maritalState,
      "starSign": writeUserInfo.starSign,
      "fileInfoIds": fileInfoIds
    }
    app.api.updateUserInfo(params)
      .then(res => {
        if (res.code == 200) {
          wx.switchTab({
            url: '../../pages/' + writeUserInfo.fromUrl + '/' + writeUserInfo.fromUrl,
          })
          wx.removeStorageSync('writeUserInfo')
        } else {
          wx.showToast({
            title: res.message ? res.message : '更新个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '更新个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  previewPhotos(e) {
    let urls = [];
    for (let i = 0; i < this.data.listData.length; i++) {
      urls.push(this.data.listData[i].url)
    }
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: urls
    })
  }
})