const app = getApp();
Page({
  data: {
    inputFocus: false,
    hiddenSelect: true,
    keyword: '', //搜索关键字
    orderPageNum: 1,
    orderIsLastPage: false,
    myOrder: null,
    pageNum: 1,
    isLastPage: false,
    listData: null,
    //筛选规则
    rule: {
      education: '',
      income: '',
      maritalState: '',
      starSign: '',
      domicileAreaId: '',
      dwellingAreaId: '',
    },
    educationData: [],
    incomeData: [],
    starSignData: [],
    maritalData: [{
      name: '单身',
      type: 'SINGLE'
    }, {
      name: '离异',
      type: 'DIVORCE'
    }, {
      name: '丧偶',
      type: 'WIDOWED'
    }],
    ageSectionArr: [],
    ageIdx: [],
    heightSectionArr: [],
    heightIdx: [],
    username: ''
  },
  onLoad: function(options) {
    this.setData({
      activityId: options.activityId
    })
    let ageSection = [],
      ageSectionArr = [];
    for (let i = 16; i < 70; i++) {
      ageSection.push(i)
    }
    ageSectionArr.push(ageSection, ageSection)
    this.setData({
      ageSectionArr: ageSectionArr
    })

    let heightSection = [],
      heightSectionArr = [];
    for (let i = 140; i < 200; i++) {
      heightSection.push(i)
    }
    heightSectionArr.push(heightSection, heightSection)
    this.setData({
      heightSectionArr: heightSectionArr
    })
    this.getEducationData()
    this.getIncomeData()
    this.getStarSignData()


    this.setData({
      pageNum: 1,
      isLastPage: false
    })
    this.getData('refresh')
  },
  onShow() {
    this.setData({
      username: app.globalData.currentUser.username
    });

    if (app.globalData.writeUserInfo && app.globalData.writeUserInfo.domicileAreaId) {
      let domicileAreaId = 'rule.domicileAreaId';
      let domicileAreas = 'rule.domicileAreas'
      this.setData({
        [domicileAreaId]: app.globalData.writeUserInfo.domicileAreaId,
        [domicileAreas]: app.globalData.writeUserInfo.domicileAreas,
      })
    }
    if (app.globalData.writeUserInfo && app.globalData.writeUserInfo.dwellingAreaId) {
      let dwellingAreaId = 'rule.dwellingAreaId';
      let dwellingAreas = 'rule.dwellingAreas'
      this.setData({
        [dwellingAreaId]: app.globalData.writeUserInfo.dwellingAreaId,
        [dwellingAreas]: app.globalData.writeUserInfo.dwellingAreas,
      })
    }
  },
  onPullDownRefresh() {
    this.setData({
      pageNum: 1,
      isLastPage: false
    })
    this.getData('refresh')
  },
  onReachBottom() {
    if (this.data.isLastPage) {
      wx.showToast({
        title: '没有更多了',
        icon: 'none',
        duration: 1000
      })
      return
    }
    let pageNum = this.data.pageNum + 1;
    this.setData({
      pageNum: pageNum
    })
    this.getData()
  },
  getData(refresh) {
    var vm = this;
    let rule = this.data.rule;
    let params = {
      current: this.data.pageNum,
      size: 10,
      name: this.data.keyword,
      minAge: this.data.ageSectionArr[0][this.data.ageIdx[0]] ? this.data.ageSectionArr[0][this.data.ageIdx[0]] : '',
      maxAge: this.data.ageSectionArr[1][this.data.ageIdx[1]] ? this.data.ageSectionArr[1][this.data.ageIdx[1]] : '',
      minHeight: this.data.heightSectionArr[0][this.data.heightIdx[0]] ? this.data.heightSectionArr[0][this.data.heightIdx[0]] : '',
      maxHeight: this.data.heightSectionArr[1][this.data.heightIdx[1]] ? this.data.heightSectionArr[1][this.data.heightIdx[1]] : '',
      education: rule.education,
      income: rule.income,
      maritalState: rule.maritalState,
      starSign: rule.starSign,
      domicileAreaId: rule.domicileAreaId,
      dwellingAreaId: rule.dwellingAreaId,
      activityId: this.data.activityId
    }
    app.api.getMatchData('match', params)
      .then(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        if (res.code == 200) {
          if (res.data) {
            let listData = [];
            if (vm.data.pageNum == 1) {
              listData = res.data.records && res.data.records.length > 0 ? res.data.records : [];
            } else {
              listData = vm.data.listData.concat(res.data.records)
            }
            let isLastPage = false;
            if (res.data.total == vm.data.pageNum * 10 || res.data.total < vm.data.pageNum * 10) {
              isLastPage = true;
            }
            vm.setData({
              listData: listData,
              isLastPage: isLastPage
            })
          } else {
            vm.setData({
              listData: []
            })
          }
        } else {
          if (res.message) {
            wx.showToast({
              title: res.message,
              icon: 'none'
            })
          }
        }
      }).catch(res => {
        if (refresh = 'refresh') {
          wx.stopPullDownRefresh();
        }
        wx.showToast({
          title: '个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  //清除搜索内容
  clearSearchKeyword() {
    if (this.data.keyword) {
      this.setData({
        inputFocus: false,
        keyword: ''
      })
      this.setData({
        pageNum: 1,
        isLastPage: false
      })
      this.getData()
    }
    this.setData({
      inputFocus: false,
    })
  },
  searchFocus() {
    this.setData({
      inputFocus: true
    })
  },
  //搜索缘分
  searchFate(e) {
    console.log(e.detail.value)
    this.setData({
      keyword: e.detail.value
    })
    //搜索数据
    this.setData({
      pageNum: 1,
      isLastPage: false
    })
    this.getData()
  },
  //显示隐藏筛选框
  selectRule() {
    this.setData({
      hiddenSelect: !this.data.hiddenSelect
    })
  },
  toDetail(e) {
    let sn = e.currentTarget.dataset.sn
    wx.navigateTo({
      url: '../../pages/activity-detail/activity-detail?sn=' + sn,
    })
  },
  toMemberDetail(e) {
    wx.navigateTo({
      url: '../../pages/member-detail/member-detail?id=' + e.currentTarget.dataset.id,
    })
  },

  /**
   * 筛选择偶条件
   * */

  //获取学历列表
  getEducationData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'EDUCATION '
      })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            educationData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '学历列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '学历列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取收入列表
  getIncomeData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'INCOME '
      })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            incomeData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '收入列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '收入列表请求出错了！',
          icon: 'none'
        })
      })
  },
  //获取星座列表
  getStarSignData() {
    var vm = this;
    app.api.getDictItem({
        typeCode: 'STAR_SIGN '
      })
      .then(res => {
        if (res.code == 200) {
          vm.setData({
            starSignData: res.data
          })
        } else {
          wx.showToast({
            title: res.message ? res.message : '星座列表请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '星座列表请求出错了！',
          icon: 'none'
        })
      })
  },
  selectMarital(e) {
    let maritalState = 'rule.maritalState'
    this.setData({
      [maritalState]: e.currentTarget.dataset.maritalstate
    })
  },
  //选择学历
  bindEducationChange: function(e) {
    let education = 'rule.education'
    this.setData({
      [education]: this.data.educationData[e.detail.value].name
    })
  },

  //选择收入
  bindIncomeChange(e) {
    let income = 'rule.income'
    this.setData({
      [income]: this.data.incomeData[e.detail.value].name
    })
  },
  //选择星座
  bindStarSignChange(e) {
    let starSign = 'rule.starSign'
    this.setData({
      [starSign]: this.data.starSignData[e.detail.value].name
    })
  },
  //选择年龄区间
  bindAgeChange: function(e) {
    this.setData({
      ageIdx: e.detail.value
    })
  },
  //选择身高区间
  bindHeightChange: function(e) {
    this.setData({
      heightIdx: e.detail.value
    })
  },
  //选择家乡
  selectDomicileAreas() {
    let domicileAreaIdArr = [];
    if (this.data.rule.domicileAreas && this.data.rule.domicileAreas.length > 0) {
      for (let i = 0; i < this.data.rule.domicileAreas.length; i++) {
        domicileAreaIdArr.push(this.data.rule.domicileAreas[i].id)
      }
    }
    wx.navigateTo({
      url: '../../pages/member-area/member-area?setAreaType=2&domicileAreaIdArr=' + JSON.stringify(domicileAreaIdArr),
    })
  },
  //选择居住地
  selectDwellingAreas() {
    let dwellingAreaIdArr = [];
    if (this.data.rule.dwellingAreas && this.data.rule.dwellingAreas.length > 0) {
      for (let i = 0; i < this.data.rule.dwellingAreas.length; i++) {
        dwellingAreaIdArr.push(this.data.rule.dwellingAreas[i].id)
      }
    }
    wx.navigateTo({
      url: '../../pages/member-area/member-area?setAreaType=1&dwellingAreaIdArr=' + JSON.stringify(dwellingAreaIdArr),
    })
  },
  //清空规则
  clearRule() {
    this.setData({
      rule: {
        education: '',
        income: '',
        maritalState: '',
        starSign: '',
        domicileAreaId: '',
        dwellingAreaId: '',
      },
      ageIdx: [],
      heightIdx: [],
    })
    app.globalData.writeUserInfo = {}
  },
  ruleSubmit() {
    this.setData({
      pageNum: 1,
      isLastPage: false,
    })
    this.getData()
    this.selectRule()
  }
})