Page({
  data: {
    wechat: ''
  },
  onLoad: function(options) {
    if (wx.getStorageSync('writeUserInfo') && wx.getStorageSync('writeUserInfo').wechat) {
      this.setData({
        wechat: wx.getStorageSync('writeUserInfo').wechat
      })
    }
  },
  changeWechat(e) {
    this.setData({
      wechat: e.detail.value
    })
  },
  save() {
    if (this.data.wechat) {
      let writeUserInfo = wx.getStorageSync('writeUserInfo') ? wx.getStorageSync('writeUserInfo') : {};
      writeUserInfo.wechat = this.data.wechat;
      wx.setStorageSync('writeUserInfo', writeUserInfo);
      wx.navigateTo({
        url: '../../pages/member-birthday/member-birthday',
      })
    } else {
      wx.showToast({
        title: '请填写微信号',
        icon: 'none'
      })
    }
  }
})