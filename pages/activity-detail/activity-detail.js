const app = getApp()
const util = require('../../utils/util.js')
Page({
  data: {
    detailData: {},
    surplusTime: '00.00',
    minute: '00',
    second: '00',
    timeComputed: null,
    sn: '',
    showModal: false,
    serviceWechat: '',
    showShareDialog: false,
    showQrcodeDialog: false,
    qrCodeType: '',
    currentEnrollStatus: '', //当前报名状态
    currentEnrollInfo: {}, //当前报名信息
    selectGoodsSn: '',
    showGoods: false
  },
  onLoad: function(options) {
    this.getData()
    this.setData({
      sn: options.sn
    })
    this.getServiceWechat()
  },
  onShow() {
    this.getData()
    this.setData({
      selectGoodsSn: '',
      showGoods: false
    })
  },
  //页面卸载时关闭计时器
  onUnload() {
    clearTimeout(this.data.timeComputed);
  },
  getData() {
    let vm = this;
    app.api.getActivityDetail(this.data.sn).then(res => {
      if (res.code == app.globalData.ok) {
        if (res.data) {
          res.data.price = res.data.price ? Number(res.data.price).toFixed(2) : '0.00';
          res.data.marketPrice = res.data.marketPrice ? Number(res.data.marketPrice).toFixed(2) : '0.00';
          if (res.data.order) {
            res.data.order.amountPayable = res.data.order.amountPayable ? Number(res.data.order.amountPayable).toFixed(2) : '0.00';
          }
          vm.setData({
            detailData: res.data
          })

          if (vm.data.detailData.order && (vm.data.detailData.order.paymentStatus == 'UNPAID' || vm.data.detailData.order.paymentStatus == 'PARTIAL_PAYMENT') && vm.data.detailData.order.expired == false) {
            vm.getTime()
          }
        }
      } else {
        wx.showToast({
          title: res.message ? res.message : '活动详情请求出错了！',
          icon: 'none'
        })
      }
    }).catch(res => {
      wx.showToast({
        title: '活动详情请求出错了！',
        icon: 'none'
      })
    })
  },
  getTime() {
    var vm = this;
    this.getRTime(vm.data.detailData.order.expire)
    let timeComputed = setInterval(function() {
      if (vm.data.minute == '00' && vm.data.second == '00') {
        clearTimeout(vm.data.timeComputed);
        return
      }
      vm.getRTime(vm.data.detailData.order.expire)
    }, 1000)
    this.setData({
      timeComputed: timeComputed
    })
  },
  closeGoodsDialog() {
    this.setData({
      showGoods: false
    })
  },
  enrollSubmit(e) {
    if (!this.data.showGoods) {
      let status = e.currentTarget.dataset.status
      let info = e.currentTarget.dataset.info
      if (status == '1') {
        this.setData({
          currentEnrollStatus: status,
          currentEnrollInfo: info,
          showGoods: true
        })
      } else {
        wx.navigateTo({
          url: '../../pages/enroll-status/enroll-status?status=' + status + '&info=' + JSON.stringify(info),
        })
      }
    } else {
      clearTimeout(this.data.timeComputed);
      wx.navigateTo({
        url: '../../pages/enroll-status/enroll-status?status=' + this.data.currentEnrollStatus + '&info=' + JSON.stringify(this.data.currentEnrollInfo) + '&selectGoodsSn=' + this.data.selectGoodsSn,
      })
    }
  },
  //订单剩余有效时间
  getRTime(EndTime) {
    // 结束时间
    var EndTime = EndTime
    //当前时间
    var NowTime = new Date();
    //后台给我的是10位 精确到秒的  所有下面我就除以了1000，不要小数点后面的
    var t = Number(EndTime) / 1000 - (NowTime.getTime() / 1000).toFixed(0);
    //如果后台给的是毫秒 上面不用除以1000  下面的计算时间也都要除以1000 这里我去掉1000了
    //天、时、分、秒
    var d = Math.floor(t / 60 / 60 / 24);
    var h = Math.floor(t / 60 / 60 % 24);
    var m = Math.floor(t / 60 % 60);
    var s = Math.floor(t % 60);
    if (parseInt(d) < 10) {
      d = "0" + d;
    }
    if (parseInt(h) < 10) {
      h = "0" + h;
    }
    if (parseInt(m) < 10) {
      m = "0" + m;
    }
    if (parseInt(s) < 10) {
      s = "0" + s;
    }
    this.setData({
      surplusTime: (h ? (h + ':') : '') + m + ':' + s,
      minute: m,
      second: s
    })
  },
  getServiceWechat() {
    let vm = this;
    app.api.getServiceWechat().then(res => {
      if (res.code == app.globalData.ok) {
        if (res.data) {
          vm.setData({
            serviceWechat: res.data
          })
        }
      } else {}
    }).catch(res => {})
  },
  // 打开弹窗
  openModal: function() {
    this.setData({
      showModal: true
    })
  },
  // 禁止屏幕滚动
  preventTouchMove: function() {},
  // 关闭弹窗
  closeModal: function() {
    this.setData({
      showModal: false
    })
  },
  copyWechat() {
    let vm = this;
    if (vm.data.serviceWechat) {
      wx.setClipboardData({
        data: vm.data.serviceWechat,
        success(res) {
          wx.getClipboardData({
            success(res) {}
          })
        }
      })
    } else {
      wx.showToast({
        title: '暂无填写微信号',
        icon: 'none'
      })
    }
  },
  // 分享
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: this.data.detailData.name,
      path: '/pages/recommend/recommend?username=' + app.globalData.currentUser.username,
      imageUrl: this.data.detailData.image
    }
  },
  toMatchMember() {
    let activityId = this.data.detailData.id
    wx.navigateTo({
      url: '../../pages/match-member/match-member?activityId=' + activityId,
    })
  },
  shareDialog() {
    this.setData({
      showShareDialog: !this.data.showShareDialog
    })
  },
  shareImg() {
    wx.navigateTo({
      url: '../../pages/share-img/share-img?detailData=' + JSON.stringify(this.data.detailData),
    })
  },
  showQrcode(e) {
    let setPhotoAuthTime = wx.getStorageSync('setPhotoAuthTime') ? wx.getStorageSync('setPhotoAuthTime') : 1;
    this.setData({
      qrCodeType: e.currentTarget.dataset.type
    })
    let url = ''
    if (this.data.qrCodeType == 1) {
      url = 'https://queqiaohui.oss-cn-hangzhou.aliyuncs.com/common/xiaochengxu.jpg'
    } else {
      url = 'https://queqiaohui.oss-cn-hangzhou.aliyuncs.com/common/qrcode.jpg'
    }
    let vm = this;
    wx.downloadFile({
      url: url,
      success(res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            vm.setData({
              showQrcodeDialog: !vm.data.showQrcodeDialog
            })
          },
          fail: function(err) {
            if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny" || err.errMsg === 'saveImageToPhotosAlbum:fail authorize no response') {
              if (setPhotoAuthTime == 1) {
                setPhotoAuthTime++
                wx.setStorageSync('setPhotoAuthTime', setPhotoAuthTime)
              } else {
                vm.openConfirmAuth()
              }
            } else {
              wx.showToast({
                title: err.errMsg,
                icon: 'none'
              })
            }
          }
        })
      }
    })
  },

  openConfirmAuth() {
    wx.showModal({
      content: '检测到您没打开保存图片权限，是否去设置打开？',
      confirmText: "确认",
      cancelText: "取消",
      success: function(res) {
        //点击“确认”时打开设置页面
        if (res.confirm) {
          wx.openSetting({
            success: (res) => {
              console.log(res)
            }
          })
        } else {
          console.log('用户点击取消')
        }
      }
    });
  },
  operateQrcodeDialog() {
    this.setData({
      showQrcodeDialog: !this.data.showQrcodeDialog
    })
  },
  // 打开活动须知
  openDisRules() {
    var vm = this;
    wx.navigateTo({
      url: '../../pages/rule/rule?title=活动须知',
    })
  },
  selectGoods(e) {
    let goods = e.currentTarget.dataset.goods;
    if (goods.sn == this.data.selectGoodsSn) {
      this.setData({
        selectGoodsSn: ''
      })
    } else {
      this.setData({
        selectGoodsSn: goods.sn
      })
    }
  }
})