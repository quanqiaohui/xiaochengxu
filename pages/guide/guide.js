// pages/guide/guide.js
const app = getApp();
Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function() {
    // 查看是否授权
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: function(res) {
              console.log(res.userInfo)
            }
          })
        }
      }
    })
  },
  toHome() {
    wx.switchTab({
      url: '../../pages/recommend/recommend'
    })
  },
  bindGetUserInfo(e) {
    console.log(e.detail.userInfo)
    app.api.updateWechatUserInfo(e.detail.userInfo).then(res => {
      wx.navigateTo({
        url: '../../pages/' + wx.getStorageSync('setUserInfoUrl') + '/' + wx.getStorageSync('setUserInfoUrl')
      })
    })
  }
})