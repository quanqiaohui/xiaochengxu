/**
 * description: 公告接口的写法，避免每个页面都写一次接口
 */
import request from './request.js'

class api {
  constructor() {
    // this._baseUrl = 'http://47.99.198.219';
    this._baseUrl ='https://www.juyoumeng.vip';
    this._defaultHeader = {
      'content-type': 'application/json'
    }
    this._request = new request
    this._request.setErrorHandler(this.errorHander)
  }

  /**
   * 统一的异常处理方法
   */
  errorHander(res) {
    console.error(res)
  }

  /**
   * 用户登录
   */
  userLogin(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/wx/user/' + data.appid + '/login', data, this._defaultHeader, true).then(res => res.data)
  }

  /**
   * 获取token
   */
  getToken(data) {
    return this._request.postRequest(this._baseUrl + '/api/user/oauth/openId/token?client_id=' + data.client_id + '&client_secret=' + data.client_secret + '&grant_type=' + data.grant_type + '&openId=' + data.openId, {}, this._defaultHeader, true).then(res => res.data)
  }

  /**
   * 刷新token
   */
  refreshToken(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/oauth/token', data, this._defaultHeader, true).then(res => res.data)
  }

  /** 当前登录用户 */
  currentUser(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/singles/currentUser', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 查询个人信息
   */
  getUserInfo(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/singles/index', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 更新个人微信信息
   */
  updateWechatUserInfo(data) {
    return this._request.putRequest(this._baseUrl + '/api/user/users', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 更新个人信息
   */
  updateUserInfo(data) {
    return this._request.putRequest(this._baseUrl + '/api/user/singles', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 更新择偶标准
   */
  updateChooseIdeals(data) {
    return this._request.putRequest(this._baseUrl + '/api/user/single/ideals', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 上传相册
   */
  uploadPhotos(url, data) {
    return this._request.postRequest(this._baseUrl + url, data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取相册图片
   */
  getPhotos(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/single/photos', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 删除相册图片
   */
  delPhotosItem(url) {
    return this._request.deleteRequest(this._baseUrl + url, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 上传头像
   */
  updateAvatar(data) {
    return this._request.putRequest(this._baseUrl + '/api/user/singles/avatar', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取活动列表
   */
  getActivityList(url, data) {
    return this._request.getRequest(this._baseUrl + '/api/marry/activities' + url, data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 查看活动详情
   */
  getActivityDetail(sn) {
    return this._request.getRequest(this._baseUrl + '/api/marry/activities/' + sn, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 支付
   */
  getPayParams(data) {
    return this._request.getRequest(this._baseUrl + '/api/marry/orders/payment', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 创建订单
   */
  createOrder(data) {
    return this._request.postRequest(this._baseUrl + '/api/marry/orders/create', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取推荐列表
   */
  getRecommendList(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/singles/recommend', data, this._defaultHeader, true).then(res => res.data)
  }

  /**
   * 获取数据词典
   */
  getDictItem(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/dict/items', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取地区列表
   */
  getArea(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/areas', data, this._defaultHeader).then(res => res.data)
  }
  /**
   * 批量上传图片
   */
  batchUpload(data) {
    return this._request.postRequest(this._baseUrl + '/api/user/file/batchUpload', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 个人详情
   */
  getMemberDetail(id) {
    return this._request.getRequest(this._baseUrl + '/api/user/tas/' + id, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 我的名片
   */
  getCardrDetail(id) {
    return this._request.getRequest(this._baseUrl + '/api/user/tas/card/' + id, {}, this._defaultHeader).then(res => res.data)
  }
  /**
   * 关注
   */
  follow(id) {
    return this._request.putRequest(this._baseUrl + '/api/user/single/follows/' + id, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 取消关注
   */
  followCancel(id) {
    return this._request.deleteRequest(this._baseUrl + '/api/user/single/follows/' + id, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 匹配列表数据
   */
  getMatchData(url, data) {
    return this._request.getRequest(this._baseUrl + '/api/user/singles/' + url, data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取活动城市
   */
  getActivityCity(data) {
    return this._request.getRequest(this._baseUrl + '/api/marry/activity/areas', data, this._defaultHeader).then(res => res.data)

  }
  /**
   * 获取客服微信号
   */
  getServiceWechat() {
    return this._request.getRequest(this._baseUrl + '/api/user/common/tmk', {}, this._defaultHeader).then(res => res.data)
  }
  /**
   * 举报
   */
  report(data) {
    return this._request.postRequest(this._baseUrl + '/api/user/single/accuses', data, this._defaultHeader).then(res => res.data)
  }

  /**
   * 查看微信号
   */
  getWechat(id) {
    return this._request.getRequest(this._baseUrl + '/api/user/ta/wechat/' + id, {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取上墙地址
   */
  getWallUrl() {
    return this._request.putRequest(this._baseUrl + '/api/user/single/wall', {}, this._defaultHeader).then(res => res.data)
  }

  /**
   * 获取地区
   */
  getAreas(data) {
    return this._request.getRequest(this._baseUrl + '/api/user/area/options', data, this._defaultHeader).then(res => res.data)
  }
  
  /**
   * 获取单身会员学历列表
   */
  getEducation() {
    return this._request.getRequest(this._baseUrl + '/api/user/single/educations', {}, this._defaultHeader).then(res => res.data)
  }
}
export default api