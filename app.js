//app.js
import api from '/apis/api.js'
App({
  onLaunch: function() {

  },
  //获取个人信息是否填写完整
  getIsHaveUserinfo(fromUrl) {
    var vm = this;
    vm.api.getUserInfo()
      .then(res => {
        console.debug(res)
        if (res.code == vm.globalData.ok) {
          let info = res.data;
          if (!info.gender) {
            console.debug('member-sex')
            wx.setStorageSync('setUserInfoUrl', 'member-sex');
            vm.toGuide(fromUrl);
          }
          else if (!info.wechat) {
            wx.setStorageSync('setUserInfoUrl', 'member-wechat');
            vm.toGuide();
          } 
          else if (!info.birth) {
            wx.setStorageSync('setUserInfoUrl', 'member-birthday');
            vm.toGuide(fromUrl);
          } else if (!info.height) {
            wx.setStorageSync('setUserInfoUrl', 'member-height');
            vm.toGuide(fromUrl);
          } else if (!info.education) {
            wx.setStorageSync('setUserInfoUrl', 'member-education');
            vm.toGuide(fromUrl);
          } else if (!info.income) {
            wx.setStorageSync('setUserInfoUrl', 'member-income');
            vm.toGuide(fromUrl);
          } else if (!info.maritalState) {
            wx.setStorageSync('setUserInfoUrl', 'member-marital');
            vm.toGuide(fromUrl);
          } else if (!info.starSign) {
            wx.setStorageSync('setUserInfoUrl', 'member-constellation');
            vm.toGuide(fromUrl);
          } else if (!info.dwellingAreaId) {
            wx.setStorageSync('setUserInfoUrl', 'member-area');
            vm.toGuide(fromUrl);
          } else if (!info.singlePhotoDtoList || info.singlePhotoDtoList && info.singlePhotoDtoList.length == 0) {
            wx.setStorageSync('setUserInfoUrl', 'member-photos');
            vm.toGuide(fromUrl);
          }
        } else {
          wx.showToast({
            title: res.message ? res.message : '个人信息请求出错了！',
            icon: 'none'
          })
        }
      }).catch(res => {
        wx.showToast({
          title: '个人信息请求出错了！',
          icon: 'none'
        })
      })
  },
  toGuide(fromUrl) {
    let writeUserInfo = wx.getStorageSync('writeUserInfo') == '' ? {} : wx.getStorageSync('writeUserInfo');
    writeUserInfo.fromUrl = fromUrl
    wx.setStorageSync('writeUserInfo', writeUserInfo)
    wx.redirectTo({
      url: '../../pages/guide/guide',
    })
  },
  globalData: {
    appid: 'wx53f06bd33f78df9d',
    client_id: 'quexiaominiapp',
    client_secret: 'quexiaominiapp520',
    ok: 200,
    open_id_grant_type: 'open_id',
    refresh_token_grant_type: 'refresh_token',
    access_token: 'accessToken',
    refresh_token: 'refreshToken',
    addPhotoImgUrl: '',
    addAvatarImgUrl: '',
    fileInfoId: '',
    writeUserInfo: {}, //填写的个人信息
    currentUser:{}, // 当前登录人信息
    matchIdx: 0
  },
  api: new api()
})