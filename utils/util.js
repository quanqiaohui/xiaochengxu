const formatTime = (value, format) => {
  var date = new Date(value);
  var y = date.getFullYear();
  var m = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
  var d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  var min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  if (format && format == 's') {
    return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s;
  } else if (format && format == '.') {
    return y + '.' + m + '.' + d;
  } else if (format && format == '/') {
    return y + '/' + m + '/' + d;
  } else if (format && format == 's.') {
    return y + '.' + m + '.' + d + ' ' + h + ':' + min + ':' + s;
  } else if (format && format == 'h.') {
    return h + ':' + min + ':' + s;
  } else {
    return y + '-' + m + '-' + d;
  }
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  formatTime: formatTime
}